from django.contrib import admin
from .models import (
    Appointment,DoctorLeave,Patient,PatientInsurance,Billing,PatientRoom,Medicine,
    Insurance,PatientReports,Disease,DoctorCharges,Reports,Room,RoomType,PatientMedicine,PatientMedicineTime
)


admin.site.register(Appointment)
admin.site.register(DoctorLeave)
admin.site.register(Patient)
admin.site.register(PatientInsurance)
admin.site.register(Billing)
admin.site.register(Insurance)
admin.site.register(PatientReports)
admin.site.register(Disease)
admin.site.register(DoctorCharges)
admin.site.register(Reports)
admin.site.register(Room)
admin.site.register(Medicine)
admin.site.register(PatientMedicine)
admin.site.register(PatientMedicineTime)
admin.site.register(RoomType)
admin.site.register(PatientRoom)
