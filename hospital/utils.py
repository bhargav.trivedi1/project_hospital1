from django.core.mail import send_mail


def appointment_cancel_mail(emails):
    send_mail(
                'Appointment',
                'Hi, Your appointment has been cancelled.As doctor is not available',
                'khakharn1234@gmail.com',
                emails,
                fail_silently=False,
            )

def appointment_accept_mail(email):
    send_mail(
                    'Appointment approval',
                    'Hi, Your appointment has accepted successfully.',
                    'khakharn1234@gmail.com',
                    [email],
                    fail_silently=False,
                )

def appointment_reject_mail(email):
    send_mail(
                    'Appointment approval',
                    'Hi, Your appointment has rejected.',
                    'khakharn1234@gmail.com',
                    [email],
                    fail_silently=False,
                )

def get_error_message_from_serializer(serializer):
    if serializer.errors:
        errors = dict(serializer.errors)
        return list(errors.values())[0][0]