from rest_framework.serializers import ValidationError

def validate_fn(patient_data):
    SpecialSym =['!', '$', '@', '#', '%']
    if not patient_data.get("first_name") or ' ' in patient_data.get("first_name") or any(char.isdigit() for char in patient_data.get("first_name")) or any(char in SpecialSym for char in patient_data.get("first_name")):
        raise ValidationError ("please enter valid patient data")
    else:
        return patient_data.get("first_name")

def validate_ln(patient_data):
    SpecialSym =['!', '$', '@', '#', '%']
    if (not patient_data.get("last_name")) or (' ' in patient_data.get("last_name")) or (any(char.isdigit() for char in patient_data.get("last_name"))) or (any(char in SpecialSym for char in patient_data.get("last_name"))):
        raise ValidationError ("please enter valid patient data")
    else:
        return patient_data.get("last_name")

def validate_email(patient_data):
    if (not patient_data.get("email")) or (' ' in patient_data.get("email")) or (not '@' in patient_data.get("email")):
        raise ValidationError ("please enter valid patient data")
    else:
        return patient_data.get("email")

def validate_age(patient_data):
    SpecialSym =['!', '$', '@', '#', '%']

    if (not str(patient_data.get("age"))) or (' ' in str(patient_data.get("age"))) or any(char.isupper() for char in str(patient_data.get("age"))) or any(char.islower() for char in str(patient_data.get("age"))) or any(char in SpecialSym for char in str(patient_data.get("age"))):
        raise ValidationError ("please enter valid patient data")
    else:
        return patient_data.get("age")

def validate_mobile(patient_data):
    SpecialSym =['!', '$', '@', '#', '%']

    if (not str(patient_data.get("mobile"))) or (' ' in str(patient_data.get("mobile"))) or any(char.isupper() for char in str(patient_data.get("mobile"))) or any(char.islower() for char in str(patient_data.get("mobile"))) or any(char in SpecialSym for char in str(patient_data.get("mobile"))) or  (not len((str(patient_data.get("mobile"))))==10):
        raise ValidationError ("please enter valid patient data")
    else:
        return patient_data.get("mobile")

def validate_insurance_amount(check_policy_cash_amount,check_policy_persentage):
    if ((not check_policy_cash_amount) and  check_policy_persentage) or ((check_policy_cash_amount) and (not check_policy_persentage )):
        return True
    else:
        return False

def check_insured_amount(delta_insured_value):
    if delta_insured_value < 0 :
        return False
    else:
        return True
         
def validate_discount_amount(discount_in_cash,discount_in_persent,total):
    if discount_in_cash > 0 and  not discount_in_persent and discount_in_cash<=total:
        return True,discount_in_cash,({"cash discount applied successfully"})
    elif discount_in_cash==0 and  discount_in_persent > 0 and discount_in_persent<100:
        return True,discount_in_persent,print({"percentage discount applied successfully"})
    else:
        return False,({"plese enter only one from cash or percentage discount"})

def validate_medicine_name(medicine_data):

    if (not medicine_data) or (' ' in medicine_data) or (any(char.isdigit() for char in medicine_data)):
        raise ValueError ("please enter valid medicine name data")
    else:
        return medicine_data

def validate_power(medicine_data1):

    if (not medicine_data1) or (' ' in medicine_data1):
        raise ValueError ("please enter valid medicine name data")
    else:
        return medicine_data1

def validate_dose(patientmedicine_data):

    if (not patientmedicine_data) or (' ' in patientmedicine_data):
        raise ValueError ("please enter valid patient medicine data")
    else:
        return patientmedicine_data
