from rest_framework import serializers
from .models import (
    Disease,Patient,Appointment,DoctorLeave,DoctorCharges,Reports,PatientReports,PatientMedicineTime,
    Billing,Room,RoomType,PatientRoom,Insurance,PatientInsurance,Medicine,PatientMedicine
)
from authentication.serializers import DoctorSerializer,NurseSerializer
from authentication.models import Doctor
import constants

class PatientViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Patient
        fields = (
            'first_name',
            'last_name',
        )

class ChoiceField(serializers.ChoiceField):

    def to_representation(self, obj):
        if obj == '' and self.allow_blank:
            return obj
        return self._choices[obj]

    def to_internal_value(self, data):
        if data == '' and self.allow_blank:
            return ''

        for key, val in self._choices.items():
            if val == data:
                return val
        self.fail('invalid_choice', input=data)

class AppointmentSerializer(serializers.ModelSerializer):
    doctor = serializers.CharField(source='doctor.user.username')
    patient = PatientViewSerializer()
    class Meta:
        model = Appointment
        fields = (
            'patient',
            'start_time',
            'end_time',
            'doctor',
            'is_accepted',
            'symptoms'

        )

class PatientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Patient
        fields = (
            'first_name',
            'last_name',
            'email',
            'mobile',
            'age',
            'gender',
            'address',  
        )
        extra_kwargs = {
            'first_name':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'blank':constants.BLANK,'max_length':constants.MAX_LENGTH,'min_length': constants.MIN_LENGTH}},
            'last_name':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'blank':constants.BLANK,'max_length':constants.MAX_LENGTH,'min_length': constants.MIN_LENGTH}},
            'email':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'blank':constants.BLANK,'max_length':constants.MAX_LENGTH,'min_length': constants.MIN_LENGTH}},
            'mobile':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'max_value':constants.MAX_VALUE,'min_value':constants.MIN_VALUE,'max_string_length': constants.MAX_STRING_LENGTH}},
            'age':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'max_value':constants.MAX_VALUE,'min_value':constants.MIN_VALUE,'max_string_length': constants.MAX_STRING_LENGTH}},
            'gender':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'blank':constants.BLANK,'max_length':constants.MAX_LENGTH,'min_length': constants.MIN_LENGTH}},
            'address':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'blank':constants.BLANK,'max_length':constants.MAX_LENGTH,'min_length': constants.MIN_LENGTH}},  

        }
        
class PatientModifySerializer(serializers.ModelSerializer):
    class Meta:
        model = Patient
        fields = (
            'doctor',
            'is_active',
            'addmited_time',
            'discharge_time',
            'is_visited',
            'disease',
            'emergency',
            'insurance'
        )

        extra_kwargs = {
            'doctor': {'error_messages': {'does_not_exist': constants.DOES_NOT_EXIST,'required ': constants.REQUIRED,'null': constants.NULL,'incorrect_type':constants.INCORRECT_TYPE}},
            'is_active':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID}},
            'addmited_time': {'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'overflow':constants.OVERFLOW,'make_aware':constants.MAKE_AWARE,'date':constants.DATE}},
            'discharge_time': {'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'overflow':constants.OVERFLOW,'make_aware':constants.MAKE_AWARE,'date':constants.DATE}},
            'is_visited': {'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID}},
            'emergency': {'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID}},
            'insurance': {'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID}},
            'insurance_claim': {'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID}},
            'disease' : {'error_messages':{'does_not_exist': constants.DOES_NOT_EXIST,'required':constants.REQUIRED,'null':constants.NULL,'not_a_list':"ijkl",'empty':"mnop"}}
        }
        
class AppointmentApprovalAddSerializer(serializers.ModelSerializer):
    class Meta:
        model = Appointment
        fields = (
            'doctor',
            'end_time',
            'is_accepted'
        )
        extra_kwargs = {
            'doctor':{'error_messages': {'does_not_exist': constants.DOES_NOT_EXIST,'required ': constants.REQUIRED,'null': constants.NULL,'incorrect_type':constants.INCORRECT_TYPE}},
            'end_time':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'overflow':constants.OVERFLOW,'make_aware':constants.MAKE_AWARE,'date':constants.DATE}},
            'is_accepted':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID}}

                }

class DiseaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Disease
        fields = (
            'disease_name',
        )
        extra_kwargs = {
            'disease_name':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'blank':constants.BLANK,'max_length':constants.MAX_LENGTH,'min_length': constants.MIN_LENGTH}}
        }    

class PatientDetailSerializer(serializers.ModelSerializer):
    doctor = serializers.CharField(source='doctor.user.username')
    gender = ChoiceField(choices=Patient.GENDER_CHOICE)
    disease = DiseaseSerializer(many = True)
    class Meta:
        model = Patient
        fields = (
            'first_name',
            'last_name',
            'email',
            'mobile',
            'age',
            'gender',
            'address', 
            'doctor', 
            'is_active',
            'addmited_time',
            'discharge_time',
            'is_visited',
            'disease'   
        )
    
class DoctorLeaveSerializer(serializers.ModelSerializer):
    class Meta:
        model = DoctorLeave
        fields = (
            'doctor',
            'leave_from',
            'leave_to',
            'leave_type',
        )
        extra_kwargs = {
            'doctor':{'error_messages': {'does_not_exist': constants.DOES_NOT_EXIST,'required ': constants.REQUIRED,'null': constants.NULL,'incorrect_type':constants.INCORRECT_TYPE}},
            'leave_from':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'overflow':constants.OVERFLOW,'make_aware':constants.MAKE_AWARE,'date':constants.DATE}},
            'leave_to':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'overflow':constants.OVERFLOW,'make_aware':constants.MAKE_AWARE,'date':constants.DATE}},
            'leave_type':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'blank':constants.BLANK,'max_length':constants.MAX_LENGTH,'min_length': constants.MIN_LENGTH}}
        }

class DoctorLeavesSerializer(serializers.ModelSerializer):
    doctor = serializers.CharField(source='doctor.user.username')
    class Meta:
        model = DoctorLeave
        fields = (
            'doctor',
            'leave_from',
            'leave_to',
            'leave_type',
        )

class DoctorChargesSerializer(serializers.ModelSerializer):
    class Meta:
        model = DoctorCharges
        fields = (
            'doctor',
            'charge',
            'emergency_charges',
            'appointment_charge'
        )
        extra_kwargs = {
            'doctor':{'error_messages': {'does_not_exist': constants.DOES_NOT_EXIST,'required ': constants.REQUIRED,'null': constants.NULL,'incorrect_type':constants.INCORRECT_TYPE}},
            'charge':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'max_value':constants.MAX_VALUE,'min_value':constants.MIN_VALUE,'max_string_length': constants.MAX_STRING_LENGTH}},
            'emergency_charges':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'max_value':constants.MAX_VALUE,'min_value':constants.MIN_VALUE,'max_string_length': constants.MAX_STRING_LENGTH}},
            'appointment_charge':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'max_value':constants.MAX_VALUE,'min_value':constants.MIN_VALUE,'max_string_length': constants.MAX_STRING_LENGTH}}

        }

class DoctorChargeSerializer(serializers.ModelSerializer):
    doctor = serializers.CharField(source='doctor.user.username')
    class Meta:
        model = DoctorCharges
        fields = (
            'doctor',
            'charge',
            'emergency_charges',
            'appointment_charge'
        )
        
class ReportSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reports
        fields = (
            'report_name',
            'charge',
            'result',
        )
        extra_kwargs = {
            'report_name':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'blank':constants.BLANK,'max_length':constants.MAX_LENGTH,'min_length': constants.MIN_LENGTH}},
            'charge':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'max_value':constants.MAX_VALUE,'min_value':constants.MIN_VALUE,'max_string_length': constants.MAX_STRING_LENGTH}},
            'result':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'blank':constants.BLANK,'max_length':constants.MAX_LENGTH,'min_length': constants.MIN_LENGTH}}
        }

class PatientReportSerializer(serializers.ModelSerializer):
    class Meta:
        model = PatientReports
        fields = (
            'patient',
            'report',
            'total_charge'
        )
        extra_kwargs = {
            'patient':{'error_messages': {'does_not_exist': constants.DOES_NOT_EXIST,'required ': constants.REQUIRED,'null': constants.NULL,'incorrect_type':constants.INCORRECT_TYPE}},
            'report':{'error_messages': {'does_not_exist': constants.DOES_NOT_EXIST,'required ': constants.REQUIRED,'null': constants.NULL,'not_a_list':constants.NOT_A_LIST,'empty':constants.EMPTY}},
            'total_charge':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'max_value':constants.MAX_VALUE,'min_value':constants.MIN_VALUE,'max_string_length': constants.MAX_STRING_LENGTH}}
        }

class PatientReportGetSerializer(serializers.ModelSerializer):
    patient = PatientViewSerializer()
    report = ReportSerializer(many=True)
    class Meta:
        model = PatientReports
        fields = (
            'patient',
            'report',
            'total_charge'
        )

class PatientReportModifySerializer(serializers.ModelSerializer):
    class Meta:
        model = PatientReports
        fields = (
            'report',
        )
        extra_kwargs = {
            'report':{'error_messages': {'does_not_exist': constants.DOES_NOT_EXIST,'required ': constants.REQUIRED,'null': constants.NULL,'not_a_list':constants.NOT_A_LIST,'empty':constants.EMPTY}}
        }

class BillSerializer(serializers.ModelSerializer):
    class Meta:
        model = Billing
        fields = (
            'patient',
            'reports',
            'doctor_charges',
            'GST',
            'discount_in_persent',
            'discount_in_cash',
            'room',
            'sum_insured'
        )
        extra_kwargs = {
            'patient':{'error_messages': {'does_not_exist': constants.DOES_NOT_EXIST,'required ': constants.REQUIRED,'null': constants.NULL,'incorrect_type':constants.INCORRECT_TYPE}},
            'reports':{'error_messages': {'does_not_exist': constants.DOES_NOT_EXIST,'required ': constants.REQUIRED,'null': constants.NULL,'incorrect_type':constants.INCORRECT_TYPE}},
            'doctor_charges':{'error_messages': {'does_not_exist': constants.DOES_NOT_EXIST,'required ': constants.REQUIRED,'null': constants.NULL,'incorrect_type':constants.INCORRECT_TYPE}},
            'GST':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'max_value':constants.MAX_VALUE,'min_value':constants.MIN_VALUE,'max_string_length': constants.MAX_STRING_LENGTH}},
            'discount_in_persent':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'max_value':constants.MAX_VALUE,'min_value':constants.MIN_VALUE,'max_string_length': constants.MAX_STRING_LENGTH}},
            'discount_in_cash':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'max_value':constants.MAX_VALUE,'min_value':constants.MIN_VALUE,'max_string_length': constants.MAX_STRING_LENGTH}},
            'room':{'error_messages': {'does_not_exist': constants.DOES_NOT_EXIST,'required ': constants.REQUIRED,'null': constants.NULL,'incorrect_type':constants.INCORRECT_TYPE}},
            'sum_insured':{'error_messages': {'does_not_exist': constants.DOES_NOT_EXIST,'required ': constants.REQUIRED,'null': constants.NULL,'incorrect_type':constants.INCORRECT_TYPE}}
        }

class RoomTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = RoomType
        fields = (
            'room_type',
        )
        extra_kwargs = {
            'room_type':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'blank':constants.BLANK,'max_length':constants.MAX_LENGTH,'min_length': constants.MIN_LENGTH}},
        }

class RoomDisplaySerializer(serializers.ModelSerializer):
    room_type = RoomTypeSerializer()
    class Meta:
        model = Room
        fields = (
            'room_number',
            'room_type',
            'charges',
        )

class RoomSerializer(serializers.ModelSerializer):
    class Meta:
        model = Room
        fields = (
            'room_number',
            'room_type',
            'charges',
        )
        extra_kwargs = {
            'room_number':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'max_value':constants.MAX_VALUE,'min_value':constants.MIN_VALUE,'max_string_length': constants.MAX_STRING_LENGTH}},
            'room_type':{'error_messages': {'does_not_exist': constants.DOES_NOT_EXIST,'required ': constants.REQUIRED,'null': constants.NULL,'incorrect_type':constants.INCORRECT_TYPE}},
            'charges':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'max_value':constants.MAX_VALUE,'min_value':constants.MIN_VALUE,'max_string_length': constants.MAX_STRING_LENGTH}}
        }

class PatientRoomViewSerializer(serializers.ModelSerializer):
    patient = PatientViewSerializer()
    room = RoomSerializer()
    class Meta:
        model = PatientRoom
        fields = (
            'patient',
            'room',
            'total_charge',
            'from_date',
            'to_date'
        )
        
class PatientRoomCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = PatientRoom
        fields = (
            'patient',
            'room',
            'from_date'
        )
        extra_kwargs = {
            'patient':{'error_messages': {'does_not_exist': constants.DOES_NOT_EXIST,'required ': constants.REQUIRED,'null': constants.NULL,'incorrect_type':constants.INCORRECT_TYPE}},
            'room':{'error_messages': {'does_not_exist': constants.DOES_NOT_EXIST,'required ': constants.REQUIRED,'null': constants.NULL,'incorrect_type':constants.INCORRECT_TYPE}},
            'from_date':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'overflow':constants.OVERFLOW,'make_aware':constants.MAKE_AWARE,'date':constants.DATE}}
        }

class PatientRoomModifySerializer(serializers.ModelSerializer):
    class Meta:
        model = PatientRoom
        fields = (
            'to_date',
        )
        extra_kwargs = {
            'to_date':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'overflow':constants.OVERFLOW,'make_aware':constants.MAKE_AWARE,'date':constants.DATE}}
        }

class InsuranceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Insurance
        fields = (
            'policy_name',
            'policy_type',
            'policy_percent',
            'policy_cash',
            'disease_covered' 
        )
        extra_kwargs = {
            'policy_name':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'blank':constants.BLANK,'max_length':constants.MAX_LENGTH,'min_length': constants.MIN_LENGTH}},
            'policy_type':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'blank':constants.BLANK,'max_length':constants.MAX_LENGTH,'min_length': constants.MIN_LENGTH}},
            'policy_percent':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'max_value':constants.MAX_VALUE,'min_value':constants.MIN_VALUE,'max_string_length': constants.MAX_STRING_LENGTH}},
            'policy_cash':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'max_value':constants.MAX_VALUE,'min_value':constants.MIN_VALUE,'max_string_length': constants.MAX_STRING_LENGTH}},
            'disease_covered':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'not_a_list':constants.NOT_A_LIST,'empty':constants.EMPTY}}
            }

class PatientInsuranceDetailSerializer(serializers.ModelSerializer):
    patient = serializers.CharField(source='patient.first_name')
    policy = serializers.CharField(source='policy.policy_name')
    class Meta:
        model = PatientInsurance
        fields = (
            'patient',
            'policy',
            'policy_number',
            'insurance_taken_at')

class PatientInsuranceCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = PatientInsurance
        fields = (
            'patient',
            'policy',
            'policy_number',
            'insurance_taken_at'
        )
        extra_kwargs = {
            'patient':{'error_messages': {'does_not_exist': constants.DOES_NOT_EXIST,'required ': constants.REQUIRED,'null': constants.NULL,'incorrect_type':constants.INCORRECT_TYPE}},
            'policy':{'error_messages': {'does_not_exist': constants.DOES_NOT_EXIST,'required ': constants.REQUIRED,'null': constants.NULL,'incorrect_type':constants.INCORRECT_TYPE}},
            'policy_number':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'max_value':constants.MAX_VALUE,'min_value':constants.MIN_VALUE,'max_string_length': constants.MAX_STRING_LENGTH}},
            'insurance_taken_at':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'overflow':constants.OVERFLOW,'make_aware':constants.MAKE_AWARE,'date':constants.DATE}}
        }

class MedicineSerializer(serializers.ModelSerializer):
    class Meta:
        model = Medicine
        fields = (
            'medicine_name',
            'power'
        )
        extra_kwargs = {
            'medicine_name':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'blank':constants.BLANK,'max_length':constants.MAX_LENGTH,'min_length': constants.MIN_LENGTH}},
            'power':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'blank':constants.BLANK,'max_length':constants.MAX_LENGTH,'min_length': constants.MIN_LENGTH}}
        }

class PatientMedicineSerializer(serializers.ModelSerializer):
    doctor_name = DoctorSerializer()
    nurse_name = NurseSerializer()
    medicine_name = MedicineSerializer()
    patient_name = PatientViewSerializer()
    class Meta:        
        model = PatientMedicine
        fields = (
            'patient_name',
            'doctor_name',
            'nurse_name',
            'medicine_name',
            'dose'
        )

class PatientMedicineCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = PatientMedicine
        fields = (
            'patient_name',
            'doctor_name',
            'nurse_name',
            'medicine_name',
            'dose'
        )
        extra_kwargs = {
            'patient_name':{'error_messages': {'does_not_exist': constants.DOES_NOT_EXIST,'required ': constants.REQUIRED,'null': constants.NULL,'incorrect_type':constants.INCORRECT_TYPE}},
            'doctor_name':{'error_messages': {'does_not_exist': constants.DOES_NOT_EXIST,'required ': constants.REQUIRED,'null': constants.NULL,'incorrect_type':constants.INCORRECT_TYPE}},
            'nurse_name':{'error_messages': {'does_not_exist': constants.DOES_NOT_EXIST,'required ': constants.REQUIRED,'null': constants.NULL,'incorrect_type':constants.INCORRECT_TYPE}},
            'medicine_name':{'error_messages': {'does_not_exist': constants.DOES_NOT_EXIST,'required ': constants.REQUIRED,'null': constants.NULL,'incorrect_type':constants.INCORRECT_TYPE}},
            'dose':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'blank':constants.BLANK,'max_length':constants.MAX_LENGTH,'min_length': constants.MIN_LENGTH}}

        }
        
class PatientMedicineTimeSerializer(serializers.ModelSerializer):
    patient_medicine = PatientMedicineSerializer()
    class Meta:
        model = PatientMedicineTime
        fields = (
            'patient_medicine',
            'date_time',
            'is_given'
        )
        
class PatientMedicineTimeCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = PatientMedicineTime
        fields = (
            'patient_medicine',
            'date_time',
            'is_given'
        )
        extra_kwargs = {
            'patient_medicine':{'error_messages': {'does_not_exist': constants.DOES_NOT_EXIST,'required ': constants.REQUIRED,'null': constants.NULL,'incorrect_type':constants.INCORRECT_TYPE}},
            'date_time':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID,'overflow':constants.OVERFLOW,'make_aware':constants.MAKE_AWARE,'date':constants.DATE}},
            'is_given':{'error_messages': {'required ': constants.REQUIRED,'null': constants.NULL,'invalid':constants.INVALID}}
        }

class BillGetSerializer(serializers.ModelSerializer):
    # patient = PatientViewSerializer()
    # reports = PatientReportGetSerializer()
    # room = PatientRoomViewSerializer()
    # doctor_charges = DoctorChargeSerializer()
    # sum_insured = PatientInsuranceDetailSerializer()
    class Meta:
        model = Billing
        fields = (
            'patient',
            'reports',
            'room',
            'doctor_charges',
            'GST',
            'discount_in_persent',
            'discount_in_cash',
            'total_amount',
            'final_amount',
            'amount_payable',
            'sum_insured'
        )