from hospital.utils import get_error_message_from_serializer
from rest_framework.response import Response

class CustomResponse:
    def __init__(self, status, http_status_code, message=None, data=None, serializer=None) -> None:
        self.status = status
        if serializer and serializer.errors:
            self.message = get_error_message_from_serializer(serializer)
        else:
            self.message = message
        if not status:
            self.data = None
        elif serializer and serializer.data and any(serializer.data.values()):
            self.data = serializer.data
        else:
            self.data = data
        self.http_status_code = http_status_code

    def get_response(self):
        return Response({
            'status': self.status,
            'message': self.message,
            'data': self.data
        }, status=self.http_status_code)
        