from django.db import models
from authentication.models import Doctor, Nurse
from django.utils import timezone
from .validation import (
    validate_fn,validate_ln,validate_email,validate_age,validate_mobile,validate_discount_amount,
    validate_medicine_name,validate_power,validate_dose
)
POLICY_TYPE = (('reemberse','reemberse'),('cashless','cashless'))


class Patient(models.Model):
    GENDER_CHOICE = (
    ('M','Male'),
    ('F','Female'),
    ('O','Others')
    )
    first_name = models.CharField(max_length=15,null=False,blank=False)
    last_name = models.CharField(max_length=15,null=False,blank=False)
    mobile = models.IntegerField()
    age = models.IntegerField()
    gender = models.CharField(max_length=3, choices=GENDER_CHOICE)
    address = models.TextField(max_length=300,null=False,blank=False)
    email = models.EmailField(null=False,blank=False)
    is_active = models.BooleanField(default=False)
    addmited_time = models.DateTimeField(null=True)
    discharge_time = models.DateTimeField(null=True)
    is_visited = models.BooleanField(blank=True,null=True)
    insurance = models.BooleanField(null=True,blank=True)
    insurance_claim = models.BooleanField(blank=True,null=True)
    disease = models.ManyToManyField('Disease')
    doctor = models.ForeignKey('authentication.Doctor',on_delete=models.CASCADE, null=True, blank=True)
    emergency = models.BooleanField(null=True,blank=True)
    
    class Meta:
        unique_together = ('first_name','last_name','email')

    
    def update_or_create_patient(data):
        try:
            patient_data = data.get("patient")
            patient = Patient.objects.get(email=validate_email(patient_data),first_name=validate_fn(patient_data), last_name=validate_ln(patient_data))
            patient.age = validate_age(patient_data)
            patient.mobile = validate_mobile(patient_data)
            patient.address = patient_data.get("address")
            patient.save()
            return False, patient

        except Patient.DoesNotExist:
            patient = Patient.objects.create(email=patient_data.get("email"),first_name=patient_data.get("first_name"), last_name=patient_data.get("last_name"),mobile = patient_data.get("mobile"),age = patient_data.get("age"),gender = patient_data.get("gender"),address = patient_data.get("address"))
            return True, patient
    def __str__(self):
        return f"{self.first_name} {self.last_name}"

class Appointment(models.Model):
    patient = models.ForeignKey(Patient,on_delete=models.CASCADE)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField(blank=True,null=True)
    doctor = models.ForeignKey('authentication.Doctor',on_delete=models.CASCADE)
    is_accepted = models.BooleanField(blank=True,null=True)
    symptoms = models.TextField()
    
    def __str__(self):
        return f"{self.patient.first_name} {self.patient.last_name}"

    def create_appointment(data):
        pass
        Appointment.objects.create(
            patient=data['patient'],
            doctor = Doctor.objects.get(pk=data.get("doctor")),
            start_time = data.get("start_time"),
            symptoms = data.get("symptoms"))
        return True

class Disease(models.Model):
    disease_name = models.CharField(max_length=20)

    def __str__(self):
        return self.disease_name

class DoctorLeave(models.Model):
    doctor = models.ForeignKey(Doctor,on_delete=models.CASCADE)
    leave_from = models.DateField()
    leave_to = models.DateField()
    leave_type = models.CharField(max_length=20)
    is_accepted =models.BooleanField(null=True,blank=True)

    def __str__(self):
        return self.leave_type

class DoctorCharges(models.Model):
    doctor = models.ForeignKey(Doctor,on_delete=models.CASCADE)
    charge = models.FloatField(null=True,blank=True)
    emergency_charges = models.FloatField(null=True,blank=True)
    appointment_charge = models.FloatField(null=True,blank=True)

    def __str__(self):
        return self.doctor.user.username

class Reports(models.Model):
    report_name = models.CharField(max_length=200)
    charge = models.FloatField()
    result = models.FileField()

    def __str__(self):
        return self.report_name

class PatientReports(models.Model):
    patient = models.ForeignKey(Patient,on_delete=models.CASCADE)
    report = models.ManyToManyField(Reports)
    total_charge = models.FloatField(null=True,blank=True)

    def __str__(self):
        return f"{ self.patient.first_name } { self.patient.last_name}"

class Billing(models.Model):
    patient = models.ForeignKey(Patient,on_delete=models.CASCADE)
    room = models.ForeignKey('PatientRoom',on_delete=models.CASCADE)
    reports = models.ForeignKey(PatientReports,on_delete=models.CASCADE)
    doctor_charges = models.ForeignKey(DoctorCharges,on_delete=models.CASCADE)
    GST = models.FloatField()
    total_amount = models.FloatField()
    discount_in_persent = models.FloatField()
    discount_in_cash = models.FloatField()
    final_amount = models.FloatField()
    sum_insured = models.ForeignKey('PatientInsurance',on_delete=models.CASCADE)
    amount_payable = models.FloatField()

    def __str__(self):
        return f"{self.patient.first_name} {self.patient.last_name}"

    def bill_calculation(data):
        pk = data.get('patient')
        patient_room = PatientRoom.objects.filter(patient=pk)
        room_charge = [data.total_charge for data in patient_room]
        room_total_charge = 0
        for ele in range(0, len(room_charge)):
            room_total_charge = room_total_charge + room_charge[ele]
        report_bill = PatientReports.objects.get(pk=data.get('reports')).total_charge
        patient = Patient.objects.get(pk=pk)
        doctor_charges = DoctorCharges.objects.get(pk=data.get('doctor_charges'))
        if patient.emergency:
            if not patient.addmited_time:
                doctor_charge = doctor_charges.emergency_charges
            else:
                days = (patient.discharge_time - patient.addmited_time).days
                charges = doctor_charges.charge*days
                doctor_charge = doctor_charges.emergency_charges - doctor_charges.charge
        else:
            if not patient.addmited_time:
                if patient.is_active:
                    doctor_charge = doctor_charges.charge
                    charges = 0
                else:
                    doctor_charge = doctor_charges.appointment_charge
                    Patient.objects.update(is_active = True)
            else:
                days = (patient.discharge_time - patient.addmited_time).days
                charges = doctor_charges.charge*days
                doctor_charge = doctor_charges.emergency_charges - doctor_charges.charge
        total = report_bill+doctor_charge + charges+room_total_charge
        print(report_bill,doctor_charge,charges,room_total_charge,total)
        gst_amount = total * data.get('GST')*0.01
        discount_in_cash = data.get('discount_in_cash')
        discount_in_persent = data.get('discount_in_persent')
        print(gst_amount,discount_in_cash,discount_in_persent)

        if validate_discount_amount(discount_in_cash,discount_in_persent,total):
            if discount_in_cash>0:
                total1 = total + gst_amount - discount_in_cash
            elif discount_in_persent>0:
                print(discount_in_persent)
                total1 = total + gst_amount - discount_in_persent*total*0.01
                print(total1)
            else:
                total1 = total + gst_amount
        sum_insured=PatientInsurance.objects.get(pk=data.get('sum_insured'))
        insurance_value = sum_insured.policy.policy_cash
        if not insurance_value:
            insurance_value = sum_insured.policy.policy_percent
        return total,total1

class Room(models.Model):
    room_number = models.IntegerField(unique=True)
    room_type = models.ForeignKey('RoomType', on_delete=models.CASCADE)
    charges = models.FloatField()
    room_book = models.BooleanField(null=True,blank=True)
    
    def __str__(self):
        return str(self.room_number)

class RoomType(models.Model):
    room_type = models.CharField(max_length=20)

    def __str__(self):
        return self.room_type

class PatientRoom(models.Model):
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE)
    room = models.ForeignKey(Room,on_delete=models.CASCADE)
    total_charge = models.FloatField(null=True)
    from_date = models.DateField(null=True,blank=True)
    to_date = models.DateField(null=True,blank=True)

    def __str__(self):
        return f"{self.patient.first_name} {self.patient.last_name}"

class Insurance(models.Model):
    policy_name = models.CharField(max_length=100,unique=True)
    policy_type = models.CharField(max_length=12,choices=POLICY_TYPE)
    policy_percent = models.FloatField(null=True,blank=True)
    policy_cash = models.FloatField(null=True,blank=True)
    disease_covered = models.ManyToManyField(Disease)
    
    def __str__(self):
        return self.policy_name

class PatientInsurance(models.Model):
    patient = models.ForeignKey(Patient,on_delete=models.CASCADE)
    policy = models.ForeignKey(Insurance,on_delete=models.CASCADE)
    policy_number = models.IntegerField()
    insurance_taken_at = models.DateTimeField()

    REQUIRED_FIELDS = [insurance_taken_at]

    def __str__(self):
        return f"{self.patient.first_name} {self.patient.last_name}"
        
    def check_insurance(data):
        insurance = Patient.objects.get(pk = data.get('patient')).insurance
        if insurance:
            if data.insurance_taken_at < timezone.now():
                return True
            else:
                return False
        else:
            return  ({"patient has no insurance policy"})

class Medicine(models.Model):
    medicine_name = models.CharField(max_length=20)
    power = models.CharField(max_length=20)

    def create_medicine(data):
        medicine_data = data.get('medicine_name')
        medicine_data1 = data.get('power')
        medicine_name = validate_medicine_name(medicine_data)
        power = validate_power(medicine_data1)

    def __str__(self):
        return self.medicine_name

class PatientMedicine(models.Model):
    patient_name = models.ForeignKey(Patient, on_delete=models.CASCADE)
    doctor_name = models.ForeignKey(Doctor, on_delete=models.CASCADE)
    nurse_name = models.ForeignKey(Nurse, on_delete=models.CASCADE)
    medicine_name = models.ForeignKey(Medicine, on_delete=models.CASCADE)
    dose = models.CharField(max_length=20)

    def create_patientmedicine(data):
        patientmedicine_data = data.get('dose')
        dose = validate_dose(patientmedicine_data)
        
    def __str__(self):
        return f"{self.patient_name.first_name} {self.patient_name.last_name}"
        
class PatientMedicineTime(models.Model):
    patient_medicine = models.ForeignKey(PatientMedicine, on_delete=models.CASCADE)
    date_time = models.DateTimeField()
    is_given = models.BooleanField()
    

    def __str__(self):
        return str(self.patient_medicine)
