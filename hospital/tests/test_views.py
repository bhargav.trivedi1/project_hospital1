from abc import ABC
import json
from authentication.models import Doctor, Nurse
from django.contrib.auth.models import User
from django.urls import reverse
from hospital.models import Disease, DoctorCharges, Insurance, Patient, PatientInsurance, PatientReports, PatientRoom, Reports, Room, RoomType
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient, APITestCase
from django.core.files.uploadedfile import SimpleUploadedFile
from django.core.files import File

class UserTests(APITestCase):

    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_superuser('admin', 'admin@admin.com', 'admin123')
        url = reverse('token_obtain_pair')
        resp = self.client.post(url, {'username':'admin', 'password':'admin123'}, format='json')
        self.doctor = Doctor.objects.create(user=self.user,mobile=7689234094,
        qualification='m.s.',
        specialization='cardio',
        experience_in_months=60,
        salary=40000
        )
        self.nurse = Nurse.objects.create(user=self.user,mobile=7689234094,
        qualification='m.s.',
        experience_in_months=60,
        salary=40000
        )
        
        self.admin_token = resp.data['access']
        
    def test_create_n_appointment(self):
        url = reverse('appointment-register')
        print(url)
        data = {'patient':  {   'first_name': 'bhargav',
      		                    'last_name':'trivedi',
           		                'email':'admin@admin.com',
           		                'mobile':8490957354,
           		                'age':26,
           		                'gender':'M',
           		                'address':'abcdefgh'},

                    'start_time':'2022-05-31 12:12:12',
                    'doctor':1,
                    'symptoms':'fever', 
                 } 
        response = self.client.post(url, data, format='json')
        print(response.data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

class PatientTest(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_superuser('bhargav', 'bhargav@trivedi.com', 'bhargav123')
        self.user1 = User.objects.create_superuser('nishit', 'bhargav@trivedi.com', 'bhargav123')
        self.disease1 = Disease.objects.create(disease_name = 'abcdefgh')
        self.doctor = Doctor.objects.create(user=self.user1,mobile=7689234094,
        qualification='m.s.',
        specialization='cardio',
        experience_in_months=60,
        salary=40000
        )
        self.patient = Patient.objects.create(
            first_name='ABC',
            last_name='DEF',
            email='abc@def',
            mobile=9227602335,
            age=23,
            gender='M',
            address='ghijklm',
            doctor=self.doctor
        )
        url = reverse('token_obtain_pair')
        resp = self.client.post(url, {'username':'nishit', 'password':'bhargav123'}, format='json')
        self.admin_token = resp.data['access']


    def test_get_patient_detail(self):
        
        url = reverse('patient')
        response = self.client.get(url, format='json', HTTP_AUTHORIZATION='Bearer '+self.admin_token)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('status'),1)
        self.assertEqual(response.data.get('message'),'display of all patients under logged in doctor')
        self.assertEqual(dict(response.data.get('data')[0]).get('first_name'),'ABC')
        self.assertEqual(dict(response.data.get('data')[0]).get('last_name'),'DEF')
        self.assertEqual(dict(response.data.get('data')[0]).get('email'),'abc@def')
        self.assertEqual(dict(response.data.get('data')[0]).get('mobile'),9227602335)
        self.assertEqual(dict(response.data.get('data')[0]).get('age'),23)
        self.assertEqual(dict(response.data.get('data')[0]).get('gender'),'Male')
        self.assertEqual(dict(response.data.get('data')[0]).get('address'),'ghijklm')
        self.assertEqual(dict(response.data.get('data')[0]).get('doctor'),'nishit')

    def test_create_disease(self):
        
        url = reverse('disease')
        data = {'disease_name': 'love'
        } 
        response = self.client.post(url, data, format='json', HTTP_AUTHORIZATION='Bearer '+self.admin_token)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data.get('status'), 1)
        self.assertEqual(response.data.get('message'), 'Disease created successfully.')
        self.assertEqual(response.data.get('data').get('disease_name'), 'love')
        
    def test_get_disease(self):
        
        url = reverse('disease')
        response_get = self.client.get(url, format='json', HTTP_AUTHORIZATION='Bearer '+self.admin_token)
        self.assertEqual(response_get.status_code, status.HTTP_200_OK)
        self.assertEqual(response_get.data.get('status'),1)
        self.assertEqual(response_get.data.get('message'),'Display-list of all disease')
        self.assertEqual(dict(response_get.data.get('data')[0]).get('disease_name'),'abcdefgh')

    def test_modify_disease(self):
        url = reverse('disease-modify',kwargs={'pk':self.disease1.pk})
        print(url)
        data = {
            'disease_name':'new disease'
            } 
        response = self.client.put(url, data, format='json', HTTP_AUTHORIZATION='Bearer '+self.admin_token)
        print(response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('status'), 1)
        self.assertEqual(response.data.get('data').get('disease_name'),'new disease')


    def test_modify_patient(self):
        url = reverse('patient-modify',kwargs={'pk': self.patient.pk})
        print(url)
        data = {
            'doctor':1,
            'is_active':1,
            'addmited_time':'2022-06-01 12:12:12',
            'discharge_time':'2022-06-02 12:12:12',
            'is_visited':1,
            'disease':[1],
            'emergency':False,
            'insurance':False       
             } 
        response = self.client.put(url, data, format='json', HTTP_AUTHORIZATION='Bearer '+self.admin_token)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('status'), 1)
        self.assertEqual(response.data.get('data').get('doctor'),1)
        self.assertEqual(response.data.get('data').get('is_active'),True)
        self.assertEqual(response.data.get('data').get('addmited_time'),'2022-06-01T12:12:12Z')
        self.assertEqual(response.data.get('data').get('discharge_time'),'2022-06-02T12:12:12Z')
        self.assertEqual(response.data.get('data').get('is_visited'),True)
        self.assertEqual(response.data.get('data').get('disease'),[1])
        self.assertEqual(response.data.get('data').get('emergency'),False)
        self.assertEqual(response.data.get('data').get('insurance'),False)
        
class InsuranceTest(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_superuser('bhargav', 'bhargav@trivedi.com', 'bhargav123')
        self.user1 = User.objects.create_superuser('nishit', 'bhargav@trivedi.com', 'bhargav123')
        self.disease1 = Disease.objects.create(disease_name = 'abcdefgh')
        self.insurance = Insurance.objects.create(
            policy_name='mediclaim',
            policy_type='cashless',
            policy_percent=0,
            policy_cash=300000)
        self.insurance.disease_covered.add(1)
        
        self.doctor = Doctor.objects.create(user=self.user1,mobile=7689234094,
        qualification='m.s.',
        specialization='cardio',
        experience_in_months=60,
        salary=40000
        )
        self.patient = Patient.objects.create(
            first_name='ABC',
            last_name='DEF',
            email='abc@def',
            mobile=9227602335,
            age=23,
            gender='M',
            address='ghijklm',
            doctor=self.doctor
        )
        self.patient_insurance = PatientInsurance.objects.create(
            patient=self.patient,
            policy=self.insurance,
            policy_number=345678,
            insurance_taken_at="2022-04-23 12:12:12"
        )
        url = reverse('token_obtain_pair')
        resp = self.client.post(url, {'username':'nishit', 'password':'bhargav123'}, format='json')
        self.admin_token = resp.data['access']

    def test_create_insurance(self):
        
        url = reverse('insurance')
        print(url)
        data = {
            'policy_name':'lic magic funds',
            'policy_type':'cashless',
            'policy_percent':0,
            'policy_cash':300000,
            'disease_covered':[1]
        } 
        response = self.client.post(url, data, format='json', HTTP_AUTHORIZATION='Bearer '+self.admin_token)
        # print(response.data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data.get('status'), 1)
        self.assertEqual(response.data.get('message'), 'insurance created successfully.')
        self.assertEqual(response.data.get('data').get('policy_name'), 'lic magic funds')
        self.assertEqual(response.data.get('data').get('policy_type'), 'cashless')
        self.assertEqual(response.data.get('data').get('policy_percent'), 0)
        self.assertEqual(response.data.get('data').get('policy_cash'), 300000)
        self.assertEqual(response.data.get('data').get('disease_covered'), [1])
        
    def test_get_insurance(self):
        
        url = reverse('insurance')
        # print(url)
        response_get = self.client.get(url, format='json', HTTP_AUTHORIZATION='Bearer '+self.admin_token)
        print(response_get.data)
        self.assertEqual(response_get.status_code, status.HTTP_200_OK)
        self.assertEqual(response_get.data.get('status'),1)
        self.assertEqual(response_get.data.get('message'),'Display-list of all insurances')
        self.assertEqual(dict(response_get.data.get('data')[0]).get('policy_name'),'mediclaim')
        self.assertEqual(dict(response_get.data.get('data')[0]).get('policy_type'),'cashless')
        self.assertEqual(dict(response_get.data.get('data')[0]).get('policy_oercent'),None)
        self.assertEqual(dict(response_get.data.get('data')[0]).get('policy_cash'),300000)
        self.assertEqual(dict(response_get.data.get('data')[0]).get('disease_covered'),[1])

    def test_modify_insurance(self):
        url = reverse('insurance-modify',kwargs={'pk': self.insurance.pk})
        # print(url)
        data = {
            'policy_name':'mediclaim',
            'policy_type':'cashless',
            'policy_percent':0,
            'policy_cash':23000,
            'disease_covered':[1]
             } 
        response = self.client.put(url, data, format='json', HTTP_AUTHORIZATION='Bearer '+self.admin_token)
        print(response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('status'), 1)
        self.assertEqual(response.data.get('message'), 'insurance modified successfully ')
        self.assertEqual(response.data.get('data').get('policy_name'),'mediclaim')
        self.assertEqual(response.data.get('data').get('policy_type'),'cashless')
        self.assertEqual(response.data.get('data').get('policy_oercent'),None)
        self.assertEqual(response.data.get('data').get('policy_cash'),23000)
        self.assertEqual(response.data.get('data').get('disease_covered'),[1])

    def test_create_patient_insurance(self):
        
        url = reverse('patient-insurance')
        # print(url)
        data = {
            'patient':1,
            'policy':1,
            'policy_number':12345,
            'insurance_taken_at':"2022-04-23 02:02:02"
        } 
        response = self.client.post(url, data, format='json', HTTP_AUTHORIZATION='Bearer '+self.admin_token)
        # print(response.data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data.get('status'), 1)
        self.assertEqual(response.data.get('message'), 'patient insurance policy is valid for claim and patient insurance object created successfully')
        self.assertEqual(response.data.get('data').get('patient'),1)
        self.assertEqual(response.data.get('data').get('policy'),1)
        self.assertEqual(response.data.get('data').get('policy_number'),12345)
        self.assertEqual(response.data.get('data').get('insurance_taken_at'),"2022-04-23T02:02:02Z")
        
    def test_get_patient_insurance(self):
        
        url = reverse('patient-insurance')
        # print(url)
        response_get = self.client.get(url, format='json', HTTP_AUTHORIZATION='Bearer '+self.admin_token)
        print(response_get.data)
        self.assertEqual(response_get.status_code, status.HTTP_200_OK)
        self.assertEqual(response_get.data.get('status'),1)
        self.assertEqual(response_get.data.get('message'),'Display of patient insurance ')
        self.assertEqual(dict(response_get.data.get('data')[0]).get('disease_covered'),None)
        self.assertEqual(dict(response_get.data.get('data')[0]).get('patient'),'ABC')
        self.assertEqual(dict(response_get.data.get('data')[0]).get('policy'),'mediclaim')
        self.assertEqual(dict(response_get.data.get('data')[0]).get('policy_number'),345678)
        self.assertEqual(dict(response_get.data.get('data')[0]).get('insurance_taken_at'),"2022-04-23T12:12:12Z")

    def test_modify_patient_insurance(self):
        url = reverse('patient-insurance-modify',kwargs={'pk': self.insurance.pk})
        # print(url)
        data = {
            'patient':1,
            'policy':1,
            'policy_number':12345,
            'insurance_taken_at':"2022-04-23 02:02:02"
             } 
        response = self.client.put(url, data, format='json', HTTP_AUTHORIZATION='Bearer '+self.admin_token)
        # print(response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('status'), 1)
        self.assertEqual(response.data.get('message'), 'patient insurance modified successfully ')
        self.assertEqual(response.data.get('data').get('patient'),1)
        self.assertEqual(response.data.get('data').get('policy'),1)
        self.assertEqual(response.data.get('data').get('policy_number'),12345)
        self.assertEqual(response.data.get('data').get('insurance_taken_at'),"2022-04-23T02:02:02Z")

class roomtest(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_superuser('admin', 'admin@admin.com', 'admin123')
        url = reverse('token_obtain_pair')
        resp = self.client.post(url, {'username':'admin', 'password':'admin123'}, format='json')
        self.doctor = Doctor.objects.create(user=self.user,mobile=7689234094,
        qualification='m.s.',
        specialization='cardio',
        experience_in_months=60,
        salary=40000
        )
        self.patient = Patient.objects.create(
            first_name='ABC',
            last_name='DEF',
            email='abc@def',
            mobile=9227602335,
            age=23,
            gender='M',
            address='ghijklm',
            doctor=self.doctor
        )
        self.nurse = Nurse.objects.create(user=self.user,mobile=7689234094,
        qualification='m.s.',
        experience_in_months=60,
        salary=40000
        )
        self.roomtype= RoomType.objects.create(room_type='air conditioned chilled')
        self.roomtype1= RoomType.objects.create(room_type='air conditioned two bed')
        self.room = Room.objects.create(room_number = 321,room_type=self.roomtype,charges=1400)
        self.patientroom = PatientRoom.objects.create(
            patient = self.patient,
            room =self.room,
            total_charge = 1000,
            from_date = "2022-06-02",
            to_date = None
        )
        file = File(open('/home/root_root/Downloads/download.pdf','rb'))
        uploaded_file = SimpleUploadedFile('download.pdf', file.read(),content_type='multipart/form-data')
        self.report = Reports.objects.create(
            report_name='MRI',
            charge=1500,
           result=uploaded_file
        )
        self.patient_report=PatientReports.objects.create(
            patient=self.patient,
        )
        self.patient_report.report.add(self.report)
        self.admin_token = resp.data['access']

    def test_create_roomtype(self):
        url = reverse('roomtype')
        # print(url)
        data = {'room_type': 'hogwardz'} 
        response = self.client.post(url, data, format='json', HTTP_AUTHORIZATION='Bearer '+self.admin_token)
        print(response.data)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data.get('status'),1)
        self.assertEqual(response.data.get('message'),'room type created successfully.')
        self.assertEqual(response.data.get('data').get('room_type'),'hogwardz')

    def test_get_roomtype(self):
        url = reverse('roomtype')
        # print(url)
        response = self.client.get(url,format='json',HTTP_AUTHORIZATION='Bearer '+self.admin_token)
        # print(response.data)

        self.assertEqual(response.status_code,status.HTTP_200_OK)

    def test_update_roomtype(self):
        url = reverse('roomtype-modify',kwargs={'pk':self.roomtype.pk})
        # print(url)
        data = {
            'room_type':'a/c chilled'
        }
        response = self.client.put(url,data,format='json',HTTP_AUTHORIZATION='Bearer '+self.admin_token)
        # print(response.data)

        self.assertEqual(response.status_code,status.HTTP_200_OK)
        self.assertEqual(response.data.get('status'),1)
        self.assertEqual(response.data.get('message'),'room type modified successfully ')
        self.assertEqual(response.data.get('data').get('room_type'),'a/c chilled')

    def test_get_room(self):
        url = reverse('room')
        # print(url)
        response = self.client.get(url,format='json',HTTP_AUTHORIZATION='Bearer '+self.admin_token)
        # print(response.data)

        self.assertEqual(response.status_code,status.HTTP_200_OK)
        self.assertEqual(response.data.get('status'),1)
        self.assertEqual(response.data.get('message'),'Display-list of all rooms')
        self.assertEqual(dict(response.data.get('data')[0]).get('room_type'),1)

    def test_create_room(self):
        url = reverse('room')
        # print(url)
        data = {
            'room_number':123,
            'room_type':1,
            'charges':1200
        } 
        response = self.client.post(url, data, format='json', HTTP_AUTHORIZATION='Bearer '+self.admin_token)
        # print(response.data)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data.get('status'),1)
        self.assertEqual(response.data.get('message'),'room created successfully.')
        self.assertEqual(response.data.get('data').get('room_type'),1)
        self.assertEqual(response.data.get('data').get('room_number'),123)
        self.assertEqual(response.data.get('data').get('charges'),1200)

    def test_update_room(self):
        url = reverse('room-modify',kwargs={'pk':self.room.pk})
        # print(url)
        data = {
            'room_number':123,
            'room_type':1,
            'charges':1200
        }
        response = self.client.put(url,data,format='json',HTTP_AUTHORIZATION='Bearer '+self.admin_token)
        # print(response.data)

        self.assertEqual(response.status_code,status.HTTP_200_OK)
        self.assertEqual(response.data.get('status'),1)
        self.assertEqual(response.data.get('message'),'room modified successfully ')
        self.assertEqual(response.data.get('data').get('room_type'),1)
        self.assertEqual(response.data.get('data').get('room_number'),123)
        self.assertEqual(response.data.get('data').get('charges'),1200)  

    def test_get_patient_room(self):
        url = reverse('patient-room')
        # print(url)
        response = self.client.get(url,format='json',HTTP_AUTHORIZATION='Bearer '+self.admin_token)
        print(response.data)

        self.assertEqual(response.status_code,status.HTTP_200_OK)
        self.assertEqual(response.data.get('status'),1)
        self.assertEqual(response.data.get('message'),'Display-list of all room of a patient')
        self.assertEqual(dict((response.data.get('data')[0]).get('patient')),{'first_name': 'ABC', 'last_name': 'DEF'})
        self.assertEqual(dict(dict(response.data.get('data')[0]).get('room')),{'room_number': 321, 'room_type': 1, 'charges': 1400.0})
        self.assertEqual(dict(response.data.get('data')[0]).get('total_charge'),1000)
        self.assertEqual(dict(response.data.get('data')[0]).get('from_date'),"2022-06-02")
        self.assertEqual(dict(response.data.get('data')[0]).get('to_date'),None)

    def test_create_patient_room(self):
        url = reverse('patient-room')
        # print(url)
        data = {
            'patient':1,
            'room':1,
            'from_date':"2022-06-02",
        } 
        response = self.client.post(url, data, format='json', HTTP_AUTHORIZATION='Bearer '+self.admin_token)
        # print(response.data)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data.get('status'),1)
        self.assertEqual(response.data.get('message'),'room of a patient posted successfully.')
        self.assertEqual(response.data.get('data').get('patient'),1)
        self.assertEqual(response.data.get('data').get('room'),1)
        self.assertEqual(response.data.get('data').get('from_date'),"2022-06-02")

    def test_update_patient_room(self):
        url = reverse('patient-room-modify',kwargs={'pk':self.patientroom.pk})
        # print(url)
        data = {
            'to_date':"2022-06-04"
        }
        response = self.client.put(url,data,format='json',HTTP_AUTHORIZATION='Bearer '+self.admin_token)
        print(response.data)

        self.assertEqual(response.status_code,status.HTTP_200_OK)
        self.assertEqual(response.data.get('status'),1)
        self.assertEqual(response.data.get('message'),'room of a patient modified successfully ')
        self.assertEqual(response.data.get('data').get('to_date'),"2022-06-04")  

class BillTest(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_superuser('bhargav', 'bhargav@trivedi.com', 'bhargav123')
        self.user1 = User.objects.create_superuser('nishit', 'bhargav@trivedi.com', 'bhargav123')
        self.disease1 = Disease.objects.create(disease_name = 'abcdefgh')
        self.insurance = Insurance.objects.create(
            policy_name='mediclaim',
            policy_type='cashless',
            policy_percent=0,
            policy_cash=300000)
        self.insurance.disease_covered.add(1)
        self.doctor = Doctor.objects.create(user=self.user1,mobile=7689234094,
        qualification='m.s.',
        specialization='cardio',
        experience_in_months=60,
        salary=40000
        )
        self.patient = Patient.objects.create(
            first_name='ABC',
            last_name='DEF',
            email='abc@def',
            mobile=9227602335,
            age=23,
            gender='M',
            address='ghijklm',
            addmited_time = '2022-05-05 12:12:12',
            discharge_time = '2022-05-08 12:12:12',
            insurance_claim = True,
            is_active = True,
            insurance = True,
            emergency = True,
            doctor=self.doctor
        )
        
        self.roomtype= RoomType.objects.create(room_type='air conditioned chilled')
        self.roomtype1= RoomType.objects.create(room_type='air conditioned two bed')
        self.room = Room.objects.create(room_number = 321,room_type=self.roomtype,charges=1400)
        self.patientroom = PatientRoom.objects.create(patient = self.patient,
            room =self.room,
            total_charge = 1000,
            from_date = "2022-06-02",
            to_date = "2022-06-03"
        )
        file = File(open('/home/root_root/Downloads/download.pdf','rb'))
        uploaded_file = SimpleUploadedFile('download.pdf', file.read(),content_type='multipart/form-data')
        self.report = Reports.objects.create(
            report_name='MRI',
            charge=1500,
           result=uploaded_file
        )
        self.patient_report=PatientReports.objects.create(
            patient=self.patient,
            total_charge = 1600
        )
        self.patient_report.report.add(self.report)
        self.doctorcharges = DoctorCharges.objects.create(
                doctor =self.doctor,
                charge = 1200,
                emergency_charges=2500,
                appointment_charge=800
        )
        self.patient_insurance = PatientInsurance.objects.create(
            patient=self.patient,
            policy=self.insurance,
            policy_number=345678,
            insurance_taken_at="2022-04-23 12:12:12"
        )
        url = reverse('token_obtain_pair')
        resp = self.client.post(url, {'username':'nishit', 'password':'bhargav123'}, format='json')
        self.admin_token = resp.data['access']

    def test_create_bill(self):
        
        url = reverse('bill')
        print(self.patient_insurance.pk,self.patient.pk,self.doctorcharges.pk)
        print(url)
        data = {
            'patient':1,
            'reports':1,
            'room':1,
            'doctor_charges':1,
            'sum_insured':1,
            'GST':8,
            'discount_in_persent':0,
            'discount_in_cash':0,
        } 
        response = self.client.post(url, data, format='json', HTTP_AUTHORIZATION='Bearer '+self.admin_token)
        print(response.data,response.data.get('discount_in_cash'))
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data.get('data').get('patient'), 1)
        self.assertEqual(response.data.get('data').get('reports'), 1)
        self.assertEqual(response.data.get('data').get('room'), 1)
        self.assertEqual(response.data.get('data').get('doctor_charges'), 1)
        self.assertEqual(response.data.get('data').get('GST'),8)
        self.assertEqual(response.data.get('discount_in_cash'),None)
        self.assertEqual(response.data.get('discount_in_persent'), None)
        self.assertEqual(response.data.get('sum_insured'),None)
        self.assertEqual(response.data.get('message'),'bill created successfully.')

        
    # def test_get_bill(self):
        
    #     url = reverse('insurance')
    #     print(url)
    #     response_get = self.client.get(url, format='json', HTTP_AUTHORIZATION='Bearer '+self.admin_token)
    #     print(response_get.data)
    #     self.assertEqual(response_get.status_code, status.HTTP_200_OK)
    #     self.assertEqual(response_get.data.get('status'),1)
    #     self.assertEqual(response_get.data.get('message'),'Display-list of all insurances')

    # def test_modify_bill(self):
    #     url = reverse('insurance-modify',kwargs={'pk': self.insurance.pk})
    #     print(url)
    #     data = {
    #         'policy_name':'mediclaim',
    #         'policy_type':'cashless',
    #         'policy_percent':0,
    #         'policy_cash':23000,
    #         'disease_covered':[1]
    #          } 
    #     response = self.client.put(url, data, format='json', HTTP_AUTHORIZATION='Bearer '+self.admin_token)
    #     print(response.data)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
    #     self.assertEqual(response.data.get('status'), 1)
    #     self.assertEqual(response.data.get('message'), 'insurance modified successfully ')