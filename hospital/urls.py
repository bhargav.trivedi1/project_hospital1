from django.urls import path
from .views import (
    DoctorChargesModifyView, PatientInsuranceView,AppointmentAcceptedView,AppointmentCancelView,LeaveCreateView,
    DoctorChargesView,ReportsModifyView,PatientView,PatientModifyView,AppointmentView,DiseaseView,BillModifyView,
    LeaveApprovalView,LeaveModifyView,ReportsView,RoomModifyView, InsuranceView, PatientInsuranceModifyView,
    PatientReportsView,PatientReportsModifyView,RoomView,RoomTypeView,DiseaseModifyView,InsuranceModifyView,
    RoomTypeModifyView,PatientRoomCreateView,PatientRoomModifyView,BillCreateView,PatientMedicineTimeDoctorView,
    AppointmentCreateView,AppointmentApprovalView,MedicineCreateView,MedicineModifyView,PatientMedicineView,
    PatientMedicineDoctorView,PatientMedicineModifyView,PatientMedicineTimeNurseView,PatientMedicineModifyTimeView
)
urlpatterns = [
   #appointment
    path('registration/',AppointmentCreateView.as_view(),name="appointment-register"),
    path('appointment/',AppointmentView.as_view(),name="appointment"),
    path('appointment/accepted/',AppointmentAcceptedView.as_view(),name="appointment-accepted"),
    path('appointment/cancel/',AppointmentCancelView.as_view(),name="appointment-cancel"),
    path('appointment/<int:pk>/',AppointmentApprovalView.as_view(),name="appointment-approval"),
   #patient
    path('patient/',PatientView.as_view(),name="patient"),
    path('patient/modify/<int:pk>/',PatientModifyView.as_view(),name="patient-modify"),
    path('disease/',DiseaseView.as_view(),name="disease"),
    path('disease/<int:pk>/modify/',DiseaseModifyView.as_view(),name="disease-modify"),
    #leaves
    path('leave/',LeaveCreateView.as_view(),name="leave"),
    path('leave/<int:pk>/',LeaveApprovalView.as_view(),name="leave-approval"),
    path('leave/modify/<int:pk>/',LeaveModifyView.as_view(),name="leave-modify"),
    #doctor charges
    path('charges/',DoctorChargesView.as_view(),name="charges"),
    path('charges/modify/<int:pk>/',DoctorChargesModifyView.as_view(),name="charges-modify"),
    #reports
    path('reports/',ReportsView.as_view(),name="report"),
    path('reports/modify/<int:pk>/',ReportsModifyView.as_view(),name="report-modify"),
    path('patient-report/',PatientReportsView.as_view(),name="patient-report"),
    path('patient-report/modify/<int:pk>/',PatientReportsModifyView.as_view(),name="patient-report-modify"),
    #rooms
    path('roomtype/', RoomTypeView.as_view(),name="roomtype"),
    path('roomtype/modify/<int:pk>/', RoomTypeModifyView.as_view(),name="roomtype-modify"),
    path('room/', RoomView.as_view(),name="room"),
    path('room/modify/<int:pk>/', RoomModifyView.as_view(),name="room-modify"),
    path('patientroom/', PatientRoomCreateView.as_view(),name="patient-room"),
    path('patientroom/modify/<int:pk>/', PatientRoomModifyView.as_view(),name="patient-room-modify"),
    #bill
    path('bill/', BillCreateView.as_view(),name="bill"),
    path('bill/modify/<int:pk>/', BillModifyView.as_view(),name="bill-modify"),
    #insurance
    path('insurance/',InsuranceView.as_view(),name="insurance"),
    path('insurance/<int:pk>/modify/',InsuranceModifyView.as_view(),name="insurance-modify"),
    path('patient/insurance/',PatientInsuranceView.as_view(),name="patient-insurance"),
    path('patient/insurance/<int:pk>/modify/',PatientInsuranceModifyView.as_view(),name="patient-insurance-modify"),
    #medicines
    path('medicine/',MedicineCreateView.as_view(),name="medicine"),
    path('medicine/modify/<int:pk>/',MedicineModifyView.as_view(),name="medicine-modify"),
    path('patient/medicine/',PatientMedicineView.as_view(),name="patient-medicine"),
    path('patient/medicine/doctor/',PatientMedicineDoctorView.as_view(),name="patient-medicine-doctor"),
    path('patient/medicine/modify/<int:pk>/',PatientMedicineModifyView.as_view(),name="patient-medicine-modify"),
    path('patient/medicine/time/doctor/',PatientMedicineTimeDoctorView.as_view(),name="patient-medicine-time-doctor"),
    path('patient/medicine/time/nurse/',PatientMedicineTimeNurseView.as_view(),name="patient-medicine-time-nurse"),
    path('patient/medicine/time/modify/<int:pk>/',PatientMedicineModifyTimeView.as_view(),name="patient-medicine-time-modify"),

]
