from authentication.models import Doctor, Nurse, Receptionist
from rest_framework.permissions import IsAuthenticated, IsAdminUser,AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import (
    AppointmentSerializer, InsuranceSerializer,PatientSerializer,DoctorChargeSerializer,BillSerializer,
    AppointmentApprovalAddSerializer,PatientDetailSerializer,PatientReportSerializer,PatientReportModifySerializer,
    DiseaseSerializer,DoctorLeaveSerializer,DoctorChargesSerializer,DoctorLeavesSerializer,ReportSerializer,
    RoomSerializer,RoomTypeSerializer,PatientRoomCreateSerializer,PatientRoomViewSerializer,PatientModifySerializer,
    PatientInsuranceDetailSerializer,PatientInsuranceCreateSerializer,PatientRoomModifySerializer,BillGetSerializer,
    MedicineSerializer,PatientMedicineSerializer,PatientMedicineCreateSerializer,PatientMedicineTimeCreateSerializer,
    PatientMedicineTimeSerializer
    )
from .response_utils import CustomResponse
import constants
from .models import (
    Appointment,Patient, Insurance, PatientInsurance,Disease,DoctorLeave,DoctorCharges,Reports,PatientReports,
    Billing,PatientRoom,Room,RoomType,PatientMedicineTime,Medicine,PatientMedicine
)
from authentication.permissions import IsNursePermission, IsReceptionistPermission,IsDoctorPermission
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST, HTTP_201_CREATED, HTTP_204_NO_CONTENT,HTTP_202_ACCEPTED
from django.db.models import Q
from django.utils import timezone
from .utils import appointment_cancel_mail,appointment_accept_mail,appointment_reject_mail
from .validation import validate_insurance_amount

class AppointmentCreateView(APIView):
    permission_classes = (AllowAny,)


    def post(self,request,*args,**kwargs):
        created, patient = Patient.update_or_create_patient(request.data)
        data = request.data
        data['patient'] = patient
        if created:
            if Appointment.create_appointment(data):
                resp = CustomResponse(status=1, http_status_code=HTTP_201_CREATED, message=constants.NEW_PATIENT_APPOINTMENT)
                return resp.get_response()
        else:
            if Appointment.create_appointment(data):
                resp = CustomResponse(status=1, http_status_code=HTTP_201_CREATED, message=constants.OLD_PATIENT_APPOINTMENT)
                return resp.get_response()
        resp = CustomResponse(status=0, http_status_code=HTTP_400_BAD_REQUEST)
        return resp.get_response()

class AppointmentAcceptedView(APIView):
    permission_classes = (IsAuthenticated,IsDoctorPermission,)
    def get(self,request,*args,**kwargs):
        queryset = Appointment.objects.filter(doctor=Doctor.objects.get(user=request.user), is_accepted=True)
        serializer = AppointmentSerializer(queryset, many=True)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.APPOINTMENT_ACCEPTED, data=serializer.data)
        return resp.get_response()
        
class AppointmentCancelView(APIView):
    permission_classes = (IsAuthenticated,IsDoctorPermission,)
    def get(self,request,*args,**kwargs):
        queryset = Appointment.objects.filter(doctor=Doctor.objects.get(user=request.user), is_accepted=False)
        serializer = AppointmentSerializer(queryset, many=True)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.APPOINTMENT_CANCELLED, data=serializer.data)
        return resp.get_response()

class AppointmentView(APIView):
    permission_classes = (IsAuthenticated,IsDoctorPermission,)
    def get(self,request,*args,**kwargs):
        queryset = Appointment.objects.filter(doctor=Doctor.objects.get(user=request.user), is_accepted=None)
        serializer = AppointmentSerializer(queryset, many=True)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.APPOINTMENT_IN_QUEUE, data=serializer.data)
        return resp.get_response()

class AppointmentApprovalView(APIView):
    permission_classes = (IsAuthenticated,IsDoctorPermission)

    def get_object(self, pk):
        try:
            return Appointment.objects.get(pk=pk)
        except Appointment.DoesNotExist:
            raise Response(status=HTTP_400_BAD_REQUEST)

    def get(self, request, pk, format=None):
        appointment= self.get_object(pk)
        serializer = AppointmentSerializer(appointment)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.APPOINTMENT_TO_BE_MODIFIED, data=serializer.data)
        return resp.get_response()

    def put(self,request,pk,*args,**kwargs):
        appointment=Appointment.objects.get(pk=pk)
        serializer = AppointmentApprovalAddSerializer(appointment,data=request.data)
        if serializer.is_valid():
            data = serializer.validated_data.get('is_accepted')
            start_time_data = appointment.start_time
            end_time_data = serializer.validated_data.get('end_time')
            start_time_overlap = Appointment.objects.filter(Q(start_time__lte=start_time_data) & Q(start_time__gte=end_time_data)).exists()
            end_time_overlap = Appointment.objects.filter(Q(end_time__lte=end_time_data) & Q(end_time__gte=start_time_data)).exists()
            time_overlap = Appointment.objects.filter(Q(start_time__lte=start_time_data) & Q(end_time__gte=end_time_data)).exists()
            if start_time_overlap or end_time_overlap or time_overlap:
                return Response({'message': 'This timing overlaps with another appointment'}, status=HTTP_400_BAD_REQUEST)
            doctor = serializer.validated_data.get('doctor')
            logged_in = Doctor.objects.get(user=request.user)
            if data :
                email = appointment.patient.email
                appointment_accept_mail(email)    
            if not data :
                email = appointment.patient.email
                appointment_reject_mail(email)
            if doctor == logged_in and serializer.validated_data.get('doctor').pk==Doctor.objects.get(user=request.user).pk:
                serializer.save()
                resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.APPOINTMENT_MODIFY_SUCCESS, serializer=serializer)
                return resp.get_response()
        resp = CustomResponse(status=0, http_status_code=HTTP_400_BAD_REQUEST,serializer=serializer)
        return resp.get_response()

class PatientView(APIView):
    permission_classes = (IsAuthenticated,IsDoctorPermission | IsReceptionistPermission)

    def get(self,request,*args,**kwargs):
        queryset = Patient.objects.filter(doctor=Doctor.objects.get(user=request.user))
        serializer = PatientDetailSerializer(queryset, many=True)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.PATIENTS_DISPLAY, data=serializer.data)
        return resp.get_response()

class PatientModifyView(APIView):
    permission_classes = (IsAuthenticated,IsReceptionistPermission|IsAdminUser)

    def get_object(self, pk):
        try:
            return Patient.objects.get(pk=pk)
        except Patient.DoesNotExist:
            raise Response(status=HTTP_400_BAD_REQUEST)

    def get(self, request, pk, format=None):
        queryset = self.get_object(pk)
        serializer = PatientDetailSerializer(queryset)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.PATIENT_UPDATE_SUCCESS,data=serializer.data)
        return resp.get_response()
    
    def put(self,request,pk,*args,**kwargs):
        queryset=Patient.objects.get(pk=pk)
        serializer = PatientModifySerializer(queryset,data=request.data)
        if serializer.is_valid():
            serializer.save()
            resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.PATIENT_UPDATE_SUCCESS, serializer=serializer)
            return resp.get_response()
        resp = CustomResponse(status=0, http_status_code=HTTP_400_BAD_REQUEST,serializer=serializer)
        return resp.get_response()

class DiseaseView(APIView):
    permission_classes = (IsAuthenticated,IsAdminUser|IsReceptionistPermission)
    
    def get(self,request,*args,**kwargs):
        queryset =Disease.objects.all()
        serializer = DiseaseSerializer(queryset,many=True)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.DISEASE_DISPLAY,data=serializer.data)
        return resp.get_response()

    def post(self,request,*args,**kwargs):
        serializer = DiseaseSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            resp = CustomResponse(status=1, http_status_code=HTTP_201_CREATED, message=constants.DISEASE_ADDED, serializer=serializer)
            return resp.get_response()
        resp = CustomResponse(status=0, http_status_code=HTTP_400_BAD_REQUEST,serializer=serializer)
        return resp.get_response()

class DiseaseModifyView(APIView):
    permission_classes = (IsAuthenticated, IsAdminUser|IsReceptionistPermission)
    def get_object(self, pk):
        try:
            return Disease.objects.get(pk=pk)
        except Disease.DoesNotExist:
            raise Response(status=HTTP_400_BAD_REQUEST)

    def get(self, request, pk, format=None):
        disease = self.get_object(pk)
        serializer = DiseaseSerializer(disease)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.DISEASE_TO_BE_MODIFIED,data=serializer.data)
        return resp.get_response()
    
    def put(self,request,pk,*args,**kwargs):
        disease=Disease.objects.get(pk=pk)
        serializer = DiseaseSerializer(disease,data=request.data)
        if serializer.is_valid():
            serializer.save()
            resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.DISEASE_MODIFY_SUCCESS, serializer=serializer)
            return resp.get_response()
        resp = CustomResponse(status=0, http_status_code=HTTP_400_BAD_REQUEST,serializer=serializer)
        return resp.get_response()

    def delete(self,request,pk,*args,**kwargs):
        disease = Disease.objects.get(pk=pk)
        disease.delete()
        return Response(status=HTTP_204_NO_CONTENT)

class LeaveCreateView(APIView):
    permission_classes = (IsAuthenticated, IsDoctorPermission)

    def get(self,request,*args,**kwargs):
        queryset = DoctorLeave.objects.filter(doctor=Doctor.objects.get(user=request.user))
        serializer = DoctorLeavesSerializer(queryset, many=True)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.LEAVE_DISPLAY, data=serializer.data)
        return resp.get_response()

    def post(self,request,*args,**kwargs):
        serializer = DoctorLeaveSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            resp = CustomResponse(status=1, http_status_code=HTTP_201_CREATED, message=constants.LEAVE_ADDED, serializer=serializer)
            return resp.get_response()
        resp = CustomResponse(status=0, http_status_code=HTTP_400_BAD_REQUEST,serializer=serializer)
        return resp.get_response()

class LeaveModifyView(APIView):
    permission_classes = (IsAuthenticated, IsDoctorPermission)
    def get_object(self, pk):
        try:
            return DoctorLeave.objects.get(pk=pk)
        except DoctorLeave.DoesNotExist:
            raise Response(status=HTTP_400_BAD_REQUEST)

    def get(self, request, pk, format=None):
        leave = self.get_object(pk)
        serializer = DoctorLeavesSerializer(leave)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.LEAVE_TO_BE_MODIFIED, data=serializer.data)
        return resp.get_response()
    
    def put(self,request,pk,*args,**kwargs):
        leave=DoctorLeave.objects.get(pk=pk)
        serializer = DoctorLeaveSerializer(leave,data=request.data)
        if serializer.is_valid():
            doctor = serializer.validated_data.get('doctor')
            logged_in = Doctor.objects.get(user=request.user)
            if doctor == logged_in and serializer.validated_data.get('doctor').pk==Doctor.objects.get(user=request.user).pk:
                serializer.save()
                resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.LEAVE_MODIFY_SUCCESS, serializer=serializer)
                return resp.get_response()
        resp = CustomResponse(status=0, http_status_code=HTTP_400_BAD_REQUEST,serializer=serializer)
        return resp.get_response()

    def delete(self,request,pk,*args,**kwargs):
        leave = DoctorLeave.objects.get(pk=pk)
        leave.delete()
        return Response(status=HTTP_204_NO_CONTENT)

class LeaveApprovalView(APIView):
    permission_classes = (IsAuthenticated, IsAdminUser)

    def get_object(self, pk):
        try:
            return DoctorLeave.objects.get(pk=pk)
        except DoctorLeave.DoesNotExist:
            raise Response(status=HTTP_400_BAD_REQUEST)

    def get(self, request, pk, format=None):
        leave = self.get_object(pk)
        serializer = DoctorLeavesSerializer(leave)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.PATIENT_UPDATE_SUCCESS, data=serializer.data)
        return resp.get_response()
    
    def put(self,request,pk,*args,**kwargs):
        leave=DoctorLeave.objects.get(pk=pk)
        appointments = Appointment.objects.filter(doctor=leave.doctor, start_time__year__gte=leave.leave_from.year,
                                                start_time__month__gte=leave.leave_from.month,
                                                start_time__day__gte=leave.leave_from.day,
                                                start_time__year__lte=leave.leave_to.year,
                                                start_time__month__lte=leave.leave_to.month,
                                                start_time__day__lte=leave.leave_to.day)
        leave.is_accepted = request.data.get('is_accepted')
        leave.save()
        if request.data.get('is_accepted'):
            appointments.update(is_accepted=False)
            emails = [data.patient.email for data in appointments]
            appointment_cancel_mail(emails)
            resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.LEAVE_ACCEPTED)
            return resp.get_response()
        else:
            resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.LEAVE_REJECTED)
            return resp.get_response()

class DoctorChargesView(APIView):
    permission_classes = (IsAuthenticated,IsAdminUser | IsReceptionistPermission)

    def get(self,request,*args,**kwargs):
        queryset =DoctorCharges.objects.all()
        serializer = DoctorChargeSerializer(queryset,many=True)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.DOCTOR_CHARGES_DISPLAY, data=serializer.data)
        return resp.get_response()

    def post(self,request,*args,**kwargs):
        serializer = DoctorChargesSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            resp = CustomResponse(status=1, http_status_code=HTTP_201_CREATED, message=constants.DOCTOR_CHARGES_ADDED, serializer=serializer)
            return resp.get_response()
        resp = CustomResponse(status=0, http_status_code=HTTP_400_BAD_REQUEST,serializer=serializer)
        return resp.get_response()

class DoctorChargesModifyView(APIView):
    permission_classes = (IsAuthenticated,IsAdminUser | IsReceptionistPermission)

    def get_object(self, pk):
        try:
            return DoctorCharges.objects.get(pk=pk)
        except DoctorCharges.DoesNotExist:
            raise Response(status=HTTP_400_BAD_REQUEST)

    def get(self, request, pk, format=None):
        queryset = self.get_object(pk)
        serializer = DoctorChargeSerializer(queryset)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.DOCTOR_CHARGES_TO_BE_MODIFIED, data=serializer.data)
        return resp.get_response()
    
    def put(self,request,pk,*args,**kwargs):
        queryset=DoctorCharges.objects.get(pk=pk)
        serializer = DoctorChargesSerializer(queryset,data=request.data)
        if serializer.is_valid():
            serializer.save()
            resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.DOCTOR_CHARGES_MODIFY_SUCCESS, serializer=serializer)
            return resp.get_response()
        resp = CustomResponse(status=0, http_status_code=HTTP_400_BAD_REQUEST,serializer=serializer)
        return resp.get_response()

class ReportsView(APIView):
    permission_classes = (IsAuthenticated,IsAdminUser | IsReceptionistPermission | IsDoctorPermission)

    def get(self,request,*args,**kwargs):
        queryset =Reports.objects.all()
        serializer = ReportSerializer(queryset,many=True)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.REPORTS_DISPLAY, data=serializer.data)
        return resp.get_response()

    def post(self,request,*args,**kwargs):
        serializer = ReportSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            resp = CustomResponse(status=1, http_status_code=HTTP_201_CREATED, message=constants.REPORTS_ADDED, serializer=serializer)
            return resp.get_response()
        resp = CustomResponse(status=0, http_status_code=HTTP_400_BAD_REQUEST,serializer=serializer)
        return resp.get_response()

class ReportsModifyView(APIView):
    permission_classes = (IsAuthenticated,IsAdminUser | IsReceptionistPermission)

    def get_object(self, pk):
        try:
            return Reports.objects.get(pk=pk)
        except Reports.DoesNotExist:
            raise Response(status=HTTP_400_BAD_REQUEST)

    def get(self, request, pk, format=None):
        queryset = self.get_object(pk)
        serializer = ReportSerializer(queryset)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.REPORTS_TO_BE_MODIFIED, data=serializer.data)
        return resp.get_response()
    
    def put(self,request,pk,*args,**kwargs):
        queryset=Reports.objects.get(pk=pk)
        serializer = ReportSerializer(queryset,data=request.data)
        if serializer.is_valid():
            serializer.save()
            resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.REPORTS_MODIFY_SUCCESS, serializer=serializer)
            return resp.get_response()
        resp = CustomResponse(status=0, http_status_code=HTTP_400_BAD_REQUEST,serializer=serializer)
        return resp.get_response()

class PatientReportsView(APIView):
    permission_classes = (IsAuthenticated,IsAdminUser | IsReceptionistPermission | IsDoctorPermission)

    def get(self,request,*args,**kwargs):
        queryset =PatientReports.objects.all()
        serializer = PatientReportSerializer(queryset,many=True)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.PATIENT_REPORTS_DISPLAY, data=serializer.data)
        return resp.get_response()

    def post(self,request,*args,**kwargs):
        serializer = PatientReportSerializer(data=request.data)
        if serializer.is_valid():
            patient = serializer.validated_data.get('patient').pk
            if PatientReports.objects.filter(patient = patient).exists():
                return Response ("patient already exists")
            else:
                serializer.save()
                resp = CustomResponse(status=1, http_status_code=HTTP_201_CREATED, message=constants.PATIENT_REPORTS_ADDED, serializer=serializer)
                return resp.get_response()
        resp = CustomResponse(status=0, http_status_code=HTTP_400_BAD_REQUEST,serializer=serializer)
        return resp.get_response()

class PatientReportsModifyView(APIView):
    permission_classes = (IsAuthenticated,IsAdminUser | IsReceptionistPermission)

    def get_object(self, pk):
        try:
            return PatientReports.objects.get(pk=pk)
        except PatientReports.DoesNotExist:
            raise Response(status=HTTP_400_BAD_REQUEST)

    def get(self, request, pk, format=None):
        queryset = self.get_object(pk)
        serializer = PatientReportSerializer(queryset)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.PATIENT_REPORTS_TO_BE_MODIFIED, data=serializer.data)
        return resp.get_response()
    
    def put(self,request,pk,*args,**kwargs):
        queryset=PatientReports.objects.get(pk=pk)
        serializer = PatientReportModifySerializer(queryset,data=request.data)
        if serializer.is_valid():
            report = serializer.validated_data.get('report')
            report_charges = [data.charge for data in report]
            total = 0
            for ele in range(0, len(report_charges)):
                total = total + report_charges[ele]
            serializer.validated_data['total_charge']=total
            serializer.save()
            resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.PATIENT_REPORTS_MODIFY_SUCCESS, serializer=serializer)
            return resp.get_response()
        resp = CustomResponse(status=0, http_status_code=HTTP_400_BAD_REQUEST,serializer=serializer)
        return resp.get_response()

class InsuranceView(APIView):
    permission_classes = (IsAuthenticated,IsReceptionistPermission | IsAdminUser)

    def get(self,request,*args,**kwargs):
        queryset =Insurance.objects.all()
        serializer = InsuranceSerializer(queryset, many=True)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.INSURANCE_DISPLAY, data=serializer.data)
        return resp.get_response()

    def post(self,request,*args,**kwargs):
        serializer = InsuranceSerializer(data=request.data)
        if serializer.is_valid():
            check_policy_cash_amount = serializer.validated_data.get("policy_cash")
            check_policy_persentage = serializer.validated_data.get("policy_percent")
            if validate_insurance_amount(check_policy_cash_amount,check_policy_persentage):
                serializer.save()
                resp = CustomResponse(status=1, http_status_code=HTTP_201_CREATED, message=constants.INSURANCE_ADDED, serializer=serializer)
                return resp.get_response()
        resp = CustomResponse(status=0, http_status_code=HTTP_400_BAD_REQUEST,serializer=serializer)
        return resp.get_response()

class InsuranceModifyView(APIView):
    permission_classes = (IsAuthenticated,IsReceptionistPermission | IsAdminUser)

    def get_object(self, pk):
        try:
            return Insurance.objects.get(pk=pk)
        except Insurance.DoesNotExist:
            raise Response(status=HTTP_400_BAD_REQUEST)

    def get(self, request, pk, format=None):
        insurance = self.get_object(pk)
        serializer = InsuranceSerializer(insurance)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.INSURANCE_TO_BE_MODIFIED, data=serializer.data)
        return resp.get_response()

    def put(self,request,pk,*args,**kwargs):
        insurance=Insurance.objects.get(pk=pk)
        serializer = InsuranceSerializer(insurance,data=request.data)
        if serializer.is_valid():
            serializer.save()
            resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.INSURANCE_MODIFY_SUCCESS, serializer=serializer)
            return resp.get_response()
        resp = CustomResponse(status=0, http_status_code=HTTP_400_BAD_REQUEST,serializer=serializer)
        return resp.get_response()

class RoomView(APIView):
    permission_classes = (IsAuthenticated, IsAdminUser | IsReceptionistPermission)
    def get(self, request, *args, **kwargs):
        queryset = Room.objects.all()
        serializer = RoomSerializer(queryset, many=True)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.ROOM_DISPLAY, data=serializer.data)
        return resp.get_response()

    def post(self, request, *args, **kwargs):
        serializer = RoomSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            resp = CustomResponse(status=1, http_status_code=HTTP_201_CREATED, message=constants.ROOM_ADDED, serializer=serializer)
            return resp.get_response()
        resp = CustomResponse(status=0, http_status_code=HTTP_400_BAD_REQUEST,message = "abc",serializer=serializer)
        return resp.get_response()
        
class RoomModifyView(APIView):
    permission_classes = (IsAuthenticated, IsAdminUser | IsReceptionistPermission)

    def get_object(self, pk):
        try:
            return Room.objects.get(pk=pk)
        except Room.DoesNotExist:
            raise Response(status=HTTP_400_BAD_REQUEST)

    def get(self, request, pk, format=None):
        room = self.get_object(pk)
        serializer = RoomSerializer(room)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.ROOM_TO_BE_MODIFIED, data=serializer.data)
        return resp.get_response()
    
    def put(self, request, pk, *args, **kwargs):
        room = Room.objects.get(pk=pk)
        serializer = RoomSerializer(room, data=request.data)
        if serializer.is_valid():
            serializer.save()
            resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.ROOM_MODIFY_SUCCESS, serializer=serializer)
            return resp.get_response()
        resp = CustomResponse(status=0, http_status_code=HTTP_400_BAD_REQUEST,serializer=serializer)
        return resp.get_response()

class RoomTypeView(APIView):
    permission_classes = (IsAuthenticated, IsAdminUser | IsReceptionistPermission)

    def get(self, request, *args, **kwargs):
        queryset = RoomType.objects.all()
        serializer = RoomTypeSerializer(queryset, many=True)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.ROOM_TYPE_DISPLAY, data=serializer.data)
        return resp.get_response()

    def post(self, request, *args, **kwargs):
        serializer = RoomTypeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            resp = CustomResponse(status=1, http_status_code=HTTP_201_CREATED, message=constants.ROOM_TYPE_ADDED, serializer=serializer)
            return resp.get_response()
        resp = CustomResponse(status=0, http_status_code=HTTP_400_BAD_REQUEST,serializer=serializer)
        return resp.get_response()

class RoomTypeModifyView(APIView):
    permission_classes = (IsAuthenticated, IsAdminUser | IsReceptionistPermission)

    def get_object(self, pk):
        try:
            return RoomType.objects.get(pk=pk)
        except RoomType.DoesNotExist:
            raise Response(status=HTTP_400_BAD_REQUEST)

    def get(self, request, pk, format=None):
        roomtype = self.get_object(pk)
        serializer = RoomTypeSerializer(roomtype)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.ROOM_TYPE_TO_BE_MODIFIED, data=serializer.data)
        return resp.get_response()

    def put(self, request, pk, *args, **kwargs):
        roomtype = RoomType.objects.get(pk=pk)
        serializer = RoomTypeSerializer(roomtype, data=request.data)
        if serializer.is_valid():
            serializer.save()
            resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.ROOM_TYPE_MODIFY_SUCCESS, serializer=serializer)
            return resp.get_response()
        resp = CustomResponse(status=0, http_status_code=HTTP_400_BAD_REQUEST,serializer=serializer)
        return resp.get_response()

class PatientRoomCreateView(APIView):
    permission_classes = (IsAuthenticated, IsAdminUser | IsReceptionistPermission )

    def get(self,request,*args,**kwargs):
        queryset = PatientRoom.objects.all()
        serializer = PatientRoomViewSerializer(queryset, many=True)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.PATIENT_ROOM_DISPLAY, data=serializer.data)
        return resp.get_response()

    def post(self,request,*args,**kwargs):
        serializer = PatientRoomCreateSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            resp = CustomResponse(status=1, http_status_code=HTTP_201_CREATED, message=constants.PATIENT_ROOM_ADDED, serializer=serializer)
            return resp.get_response()
        resp = CustomResponse(status=0, http_status_code=HTTP_400_BAD_REQUEST,serializer=serializer)
        return resp.get_response()

class PatientRoomModifyView(APIView):
    permission_classes = (IsAuthenticated, IsAdminUser | IsReceptionistPermission )

    def get_object(self, pk):
        try:
            return PatientRoom.objects.get(pk=pk)
        except PatientRoom.DoesNotExist:
            raise Response(status=HTTP_400_BAD_REQUEST)

    def get(self, request, pk, format=None):
        rooms = self.get_object(pk)
        serializer = PatientRoomViewSerializer(rooms)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.PATIENT_ROOM_TO_BE_MODIFIED, data=serializer.data)
        return resp.get_response()

    def put(self,request,pk,*args,**kwargs):
        patient_room=PatientRoom.objects.get(pk=pk)
        serializer = PatientRoomModifySerializer(patient_room,data=request.data)
        if serializer.is_valid():
            to_date = serializer.validated_data.get('to_date')
            if to_date > patient_room.from_date :
                days = (to_date - patient_room.from_date).days
                total = patient_room.room.charges*days
                serializer.validated_data['total_charge'] = total
                serializer.save()
                resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.PATIENT_ROOM_MODIFY_SUCCESS, serializer=serializer)
                return resp.get_response()
        resp = CustomResponse(status=0, http_status_code=HTTP_400_BAD_REQUEST,serializer=serializer)
        return resp.get_response()

class BillCreateView(APIView):
    permission_classes = (IsAuthenticated, IsAdminUser | IsReceptionistPermission )

    def get(self,request,*args,**kwargs):
        queryset = Billing.objects.all()
        serializer = BillGetSerializer(queryset, many=True)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.BILL_DISPLAY, data=serializer.data)
        return resp.get_response()

    def post(self,request,*args,**kwargs):
        serializer = BillSerializer(data=request.data)
        if serializer.is_valid():
            total_amount1,final_amount1 = Billing.bill_calculation(request.data)
            serializer.validated_data['total_amount'] =  total_amount1
            serializer.validated_data['final_amount'] =  final_amount1
            serializer.save()
            resp = CustomResponse(status=1, http_status_code=HTTP_201_CREATED, message=constants.BILL_ADDED, serializer=serializer)
            return resp.get_response()
        resp = CustomResponse(status=0, http_status_code=HTTP_400_BAD_REQUEST,serializer=serializer)
        return resp.get_response()

class BillModifyView(APIView):
    permission_classes = (IsAuthenticated, IsAdminUser | IsReceptionistPermission )

    def get_object(self, pk):
        try:
            return Billing.objects.get(pk=pk)
        except Billing.DoesNotExist:
            resp = CustomResponse(status=0, http_status_code=HTTP_400_BAD_REQUEST)
            raise resp.get_response()

    def get(self, request, pk, format=None):
        bill = self.get_object(pk)
        serializer = BillSerializer(bill)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.BILL_TO_BE_MODIFIED, data=serializer.data)
        return resp.get_response()

    def put(self,request,pk,*args,**kwargs):
        bill=Billing.objects.get(pk=pk)
        serializer = BillSerializer(bill,data=request.data)
        if serializer.is_valid():
            serializer.save()
            resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.BILL_MODIFY_SUCCESS, serializer=serializer)
            return resp.get_response()
        resp = CustomResponse(status=0, http_status_code=HTTP_400_BAD_REQUEST,serializer=serializer)
        return resp.get_response()

class PatientInsuranceView(APIView):
    permission_classes = (IsAuthenticated,IsReceptionistPermission | IsAdminUser)

    def get(self,request,*args,**kwargs):
        queryset =PatientInsurance.objects.all()
        serializer = PatientInsuranceDetailSerializer(queryset, many=True)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.PATIENT_INSURANCE_DISPLAY, data=serializer.data)
        return resp.get_response()

    def post(self,request,*args,**kwargs):
        serializer = PatientInsuranceCreateSerializer(data=request.data)
        if serializer.is_valid():
            if PatientInsurance.check_insurance(request.data):
                serializer.save()
                resp = CustomResponse(status=1, http_status_code=HTTP_201_CREATED, message=constants.INSURANCE_VALID, serializer=serializer)
                return resp.get_response()
            else:
                serializer.save()
                resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.INSURANCE_INVALID, serializer=serializer)
                return resp.get_response()
        resp = CustomResponse(status=0, http_status_code=HTTP_400_BAD_REQUEST,serializer=serializer)
        return resp.get_response()

class PatientInsuranceModifyView(APIView):
    permission_classes = (IsAuthenticated,IsReceptionistPermission | IsAdminUser)

    def get_object(self, pk):
        try:
            return PatientInsurance.objects.get(pk=pk)
        except PatientInsurance.DoesNotExist:
            resp = CustomResponse(status=0, http_status_code=HTTP_400_BAD_REQUEST)
            return resp.get_response()

    def get(self, request, pk, format=None):
        insurance = self.get_object(pk)
        serializer = ReportSerializer(insurance)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.PATIENT_INSURANCE_TO_BE_MODIFIED, data=serializer.data)
        return resp.get_response()

    def put(self,request,pk,*args,**kwargs):
        insurance=PatientInsurance.objects.get(pk=pk)
        serializer = PatientInsuranceCreateSerializer(insurance,data=request.data)
        if serializer.is_valid():
            serializer.save()
            resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.PATIENT_INSURANCE_MODIFY_SUCCESS, serializer=serializer)
            return resp.get_response()
        resp = CustomResponse(status=0, http_status_code=HTTP_400_BAD_REQUEST,serializer=serializer)
        return resp.get_response()

class MedicineCreateView(APIView):
    permission_classes = ( IsAuthenticated,IsDoctorPermission)

    def get(self, request, *args, **kwargs):
        queryset = Medicine.objects.all()
        serializer = MedicineSerializer(queryset, many=True)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.MEDICINE_DISPLAY, data=serializer.data)
        return resp.get_response()

    def post(self, request, *args, **kwargs):
        serializer = MedicineSerializer(data=request.data)
        if serializer.is_valid():
            Medicine.create_medicine(request.data)
            medicine_name = serializer.validated_data.get('medicine_name')
            if Medicine.objects.filter(medicine_name=medicine_name).exists():
                return Response('medicine already exits')
            else:
                serializer.save()
                resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.MEDICINE_ADDED, serializer=serializer)
                return resp.get_response()
        resp = CustomResponse(status=0, http_status_code=HTTP_400_BAD_REQUEST,serializer=serializer)
        return resp.get_response()

class MedicineModifyView(APIView):
    permission_classes = (IsAuthenticated,IsDoctorPermission)

    def get_object(self, pk):
        try:
            return Medicine.objects.get(pk=pk)
        except Medicine.DoesNotExist:
            raise Response(status=HTTP_400_BAD_REQUEST)

    def get(self, request, pk, format=None):
        Medicine = self.get_object(pk)
        serializer = MedicineSerializer(Medicine)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.MEDICINE_TO_BE_MODIFIED, data=serializer.data)
        return resp.get_response()

    def put(self, request, pk, *args, **kwargs):
        medicine = Medicine.objects.get(pk=pk)
        serializer = MedicineSerializer(medicine, data=request.data)
        if serializer.is_valid():
            Medicine.create_medicine(request.data)
            medicine_name = serializer.validated_data.get('medicine_name')
            if Medicine.objects.filter(medicine_name=medicine_name).exists():
                return Response('medicine already exits')
            serializer.save()
            resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.MEDICINE_MODIFY_SUCCESS, serializer=serializer)
            return resp.get_response()
        resp = CustomResponse(status=0, http_status_code=HTTP_400_BAD_REQUEST,serializer=serializer)
        return resp.get_response()

class PatientMedicineView(APIView):
    permission_classes = (IsAuthenticated,IsAdminUser | IsReceptionistPermission | IsDoctorPermission)

    def get(self,request,*args,**kwargs):
        queryset = PatientMedicine.objects.all()
        serializer = PatientMedicineSerializer(queryset, many=True)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.ALL_PATIENT_MEDICINE_DISPLAY, data=serializer.data)
        return resp.get_response()

class PatientMedicineDoctorView(APIView):
    permission_classes = (IsAuthenticated, IsDoctorPermission)

    def get(self,request,*args,**kwargs):
        queryset = PatientMedicine.objects.filter(doctor_name=Doctor.objects.get(user=request.user))
        serializer = PatientMedicineSerializer(queryset, many=True)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.PATIENT_MEDICINE_DISPLAY, data=serializer.data)
        return resp.get_response()

    def post(self,request,*args,**kwargs):
        serializer = PatientMedicineCreateSerializer(data=request.data)
        if serializer.is_valid():
            PatientMedicine.create_patientmedicine(request.data)
            serializer.save()
            resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.PATIENT_MEDICINE_ADDED, serializer=serializer)
            return resp.get_response()
        resp = CustomResponse(status=0, http_status_code=HTTP_400_BAD_REQUEST,serializer=serializer)
        return resp.get_response()

class PatientMedicineModifyView(APIView):
    permission_classes = (IsAuthenticated, IsDoctorPermission )

    def get_object(self, pk):
        try:
            return PatientMedicine.objects.get(pk=pk)
        except PatientMedicine.DoesNotExist:
            raise Response(status=HTTP_400_BAD_REQUEST)

    def get(self, request, pk, format=None):
        PatientMedicine = self.get_object(pk)
        serializer = PatientMedicineSerializer(PatientMedicine)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.PATIENT_MEDICINE_TO_BE_MODIFIED, data=serializer.data)
        return resp.get_response()

    def put(self,request,pk,*args,**kwargs):
        patientmedicine=PatientMedicine.objects.get(pk=pk)
        serializer = PatientMedicineCreateSerializer(patientmedicine,data=request.data)
        if serializer.is_valid():
            doctor = serializer.validated_data.get('doctor_name')
            logged_in = Doctor.objects.get(user=request.user)
            if doctor == logged_in:
                serializer.save()
                resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.PATIENT_MEDICINE_MODIFY_SUCCESS, serializer=serializer)
                return resp.get_response()
        resp = CustomResponse(status=0, http_status_code=HTTP_400_BAD_REQUEST,serializer=serializer)
        return resp.get_response()
    
class PatientMedicineTimeNurseView(APIView):
    permission_classes = (IsAuthenticated, IsNursePermission)

    def get(self,request,*args,**kwargs):
        queryset = PatientMedicineTime.objects.filter(patient_medicine__in=
        PatientMedicine.objects.filter(nurse_name=Nurse.objects.get(user=request.user)))
        serializer = PatientMedicineTimeSerializer(queryset, many=True)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.PATIENT_MEDICINE_TIME_DISPLAY_NURSE, data=serializer.data)
        return resp.get_response()

    def post(self,request,*args,**kwargs):
        serializer = PatientMedicineTimeCreateSerializer(data=request.data)
        if serializer.is_valid():
            nurse = serializer.validated_data.get('patient_medicine').nurse_name
            logged_in = Nurse.objects.get(user=request.user)
            if nurse == logged_in:
                serializer.save()
                resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.PATIENT_MEDICINE_TIME_ADDED, serializer=serializer)
                return resp.get_response()
        resp = CustomResponse(status=0, http_status_code=HTTP_400_BAD_REQUEST,serializer=serializer)
        return resp.get_response()

class PatientMedicineTimeDoctorView(APIView):
    permission_classes = (IsAuthenticated, IsDoctorPermission)

    def get(self,request,*args,**kwargs):
        queryset = PatientMedicineTime.objects.filter(patient_medicine__in=PatientMedicine.objects.filter(doctor_name=Doctor.objects.get(user=request.user)))
        serializer = PatientMedicineTimeSerializer(queryset, many=True)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.PATIENT_MEDICINE_TIME_DISPLAY_DOCTOR, data=serializer.data)
        return resp.get_response()

class PatientMedicineModifyTimeView(APIView):
    permission_classes = (IsAuthenticated, IsNursePermission)

    def get_object(self, pk):
        try:
            return PatientMedicineTime.objects.get(pk=pk)
        except PatientMedicineTime.DoesNotExist:
            raise Response(status=HTTP_400_BAD_REQUEST)

    def get(self, request, pk, format=None):
        patientmedicinetime = self.get_object(pk)
        serializer = PatientMedicineTimeSerializer(patientmedicinetime)
        resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.PATIENT_MEDICINE_TIME_TO_BE_MODIFIED, data=serializer.data)
        return resp.get_response()

    def put(self,request,pk,*args,**kwargs):
        patientmedicinetime=PatientMedicineTime.objects.get(pk=pk)
        serializer = PatientMedicineTimeCreateSerializer(patientmedicinetime,data=request.data)
        if serializer.is_valid():
            nurse = serializer.validated_data.get('patient_medicine').nurse_name
            logged_in = Nurse.objects.get(user=request.user)
            if nurse == logged_in:
                serializer.save()
                resp = CustomResponse(status=1, http_status_code=HTTP_200_OK, message=constants.PATIENT_MEDICINE_TIME_MODIFY_SUCCESS, serializer=serializer)
                return resp.get_response()
        resp = CustomResponse(status=0, http_status_code=HTTP_400_BAD_REQUEST,serializer=serializer)
        return resp.get_response()
