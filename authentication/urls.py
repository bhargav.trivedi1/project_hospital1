from django.urls import path
from .views import UserSignUpView,DoctorSignUpView,NurseSignUpView,ReceptionistSignUpView


urlpatterns = [
    path('registration/',UserSignUpView.as_view(),name="register-user"),
    path('doctor/',DoctorSignUpView.as_view(),name="doctor"),
    path('nurse/',NurseSignUpView.as_view(),name="nurse"),
    path('receptionist/',ReceptionistSignUpView.as_view(),name="receptionist"),
]
