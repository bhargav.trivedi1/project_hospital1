from rest_framework import serializers
from .models import Doctor,Nurse, Receptionist
from django.contrib.auth.models import User

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'first_name',
            'last_name',
            'username',
            'email',
            'password'
        )

    def create(self, validated_data):
        user = super().create(validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user


class DoctorSerializer(serializers.ModelSerializer):
    # user = UserSerializer()
    class Meta:
        model = Doctor
        fields = (
            'user',
            'mobile',
            'qualification',
            'specialization',
            'experience_in_months',
            'salary'
        )

class ReceptionistSerializer(serializers.ModelSerializer):
    class Meta:
        model = Receptionist
        fields = (
            'user',
            'mobile',
            'qualification',
            'experience_in_months',
            'salary'
        )


    # def create(self, validated_data):
    #     user_email = validated_data.pop('user')
    #     user = User.objects.get(email=user_email.get('email'))
    #     doctor = Doctor.objects.create(user=user, **validated_data)
    #     return doctor

class NurseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Nurse
        fields = (
            'user',
            'mobile',
            'qualification',
            'experience_in_months',
            'salary'
        )

