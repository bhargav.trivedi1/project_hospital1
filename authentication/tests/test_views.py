from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.test import APIClient
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User
from authentication.models import Doctor, Nurse

class UserTests(APITestCase):

    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_superuser('admin', 'admin@admin.com', 'admin123')
        url = reverse('token_obtain_pair')
        resp = self.client.post(url, {'username':'admin', 'password':'admin123'}, format='json')
        self.doctor = Doctor.objects.create(user=self.user,mobile=7689234094,
        qualification='m.s.',
        specialization='cardio',
        experience_in_months=60,
        salary=40000
        )
        self.nurse = Nurse.objects.create(user=self.user,mobile=7689234094,
        qualification='m.s.',
        experience_in_months=60,
        salary=40000
        )
        
        self.admin_token = resp.data['access']


    def test_user_signup(self):
        
        url = reverse('register-user')
        data = {'first_name': 'test', 
                'last_name': 'test',
                'username':'ketrrina', 
                'email':'test@test.com', 
                'password': 'abdc@1243'}
        response = self.client.post(url, data, format='json', HTTP_AUTHORIZATION='Bearer '+self.admin_token)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        
      