from django.contrib import admin
from .models import Doctor,Nurse,Worker,Receptionist


admin.site.register(Doctor)
admin.site.register(Nurse)
admin.site.register(Worker)
admin.site.register(Receptionist)