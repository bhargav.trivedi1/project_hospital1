from django.db import models
from django.contrib.auth.models import User


class Doctor(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    mobile = models.IntegerField(null=False)    
    qualification = models.CharField(max_length=100)
    specialization = models.CharField(max_length=100)
    salary = models.FloatField()
    experience_in_months = models.IntegerField()
    

    def __str__(self):
        return self.user.username

class Nurse(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    experience_in_months = models.IntegerField()
    qualification = models.CharField(max_length=100)
    mobile = models.IntegerField(null=False)
    salary = models.FloatField()


    def __str__(self):
        return self.user.username

class Worker(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    department = models.CharField(max_length=100)
    shift = models.CharField(max_length=100 )
    mobile = models.IntegerField(null=False)

    def __str__(self):
        return self.user.username

class Receptionist(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    experience_in_months = models.IntegerField()
    qualification = models.CharField(max_length=100)
    mobile = models.IntegerField(null=False)
    salary = models.FloatField()


    def __str__(self):
        return self.user.username
