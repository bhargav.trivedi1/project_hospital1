from django.contrib.auth.models import User
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from authentication.models import Doctor,Receptionist,Nurse
from .permissions import IsDoctorPermission,IsNursePermission,IsReceptionistPermission
from .serializers import ReceptionistSerializer, UserSerializer,DoctorSerializer,NurseSerializer
from rest_framework.generics import CreateAPIView

class UserSignUpView(CreateAPIView):
    permission_classes = (IsAuthenticated, IsAdminUser)
    serializer_class = UserSerializer
    queryset = User.objects.all()

class DoctorSignUpView(CreateAPIView):
    permission_classes = (IsAuthenticated,IsAdminUser | IsReceptionistPermission)
    serializer_class = DoctorSerializer
    queryset = Doctor.objects.all()

class ReceptionistSignUpView(CreateAPIView):
    permission_classes = (IsAuthenticated,IsAdminUser)
    serializer_class = ReceptionistSerializer
    queryset = Receptionist.objects.all()
    
class NurseSignUpView(CreateAPIView):
    permission_classes = (IsAuthenticated,IsAdminUser | IsReceptionistPermission)
    serializer_class = NurseSerializer
    queryset = Nurse.objects.all()

