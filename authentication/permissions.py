from rest_framework import permissions
from authentication.models import Doctor, Nurse,Receptionist


class IsDoctorPermission(permissions.BasePermission):

    def has_permission(self, request,view):
        return Doctor.objects.filter(user=request.user).exists()


class IsNursePermission(permissions.BasePermission):

    def has_permission(self, request, view):    
        return Nurse.objects.filter(user=request.user).exists()


class IsReceptionistPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        return Receptionist.objects.filter(user=request.user).exists()