# ==================== ERROR MASSEGES  =========================
DOES_NOT_EXIST = 'wrong pk.enter pk of existing field object'
REQUIRED = 'this field is required'
NULL = 'please enter correct data,this field can not be null'
INCORRECT_TYPE = 'please enter intiger type value for pk field'
INVALID = "Please enter valid input data"
OVERFLOW = "datetime values are out of range for month and days"
MAKE_AWARE = "invalid datetime for timezone "
DATE = "'addmited_time expected to be datetime but got a date instead'"
NOT_A_LIST = "many to many field must be list"
EMPTY = "manytomany field can not be empty"
MAX_VALUE = "entered value is more then maximum value of the field"
MIN_VALUE = "entered value is less then minimum value of the field"
MAX_STRING_LENGTH = "string length is more the maximum length"
BLANK = "please enter correct data,this field can not be blank"
MAX_LENGTH =  "entered value is more then maximum length of the field"
MIN_LENGTH = "entered value is more then minimum length of the field"

# ===================== patient apis =============================
PATIENT_UPDATE_SUCCESS = 'Patient modified successfully.'
PATIENTS_DISPLAY = 'display of all patients under logged in doctor'
PATIENT_TO_BE_MODIFIED = "Data of a patient with given pk "
PATIENT_DISPLAY_PK = 'display of perticular petient(pk=pk)'

# ===================== PatientInsurance ==========================
INSURANCE_VALID = "patient insurance policy is valid for claim and patient insurance object created successfully"
INSURANCE_INVALID = "patient insurance policy is invalid for claim and patient insurance object can not be created"

# ====================== Appointment ========================
NEW_PATIENT_APPOINTMENT = "patient appointment created successfully with new patient"
OLD_PATIENT_APPOINTMENT = "patient appointment created successfully with old patient"
APPOINTMENT_ACCEPTED = "List of accepted appointments"
APPOINTMENT_CANCELLED = "List of cancelled appointments"
APPOINTMENT_TO_BE_MODIFIED = "Data of an appointment with given pk "
APPOINTMENT_MODIFY_SUCCESS = "Appointment modified successfully "
APPOINTMENT_IN_QUEUE = "List of pending appointments"

# ===================== disease apis =============================
DISEASE_DISPLAY = 'Display-list of all disease'
DISEASE_TO_BE_MODIFIED = "Data of a disease with given pk "
DISEASE_ADDED = "Disease created successfully."
DISEASE_MODIFY_SUCCESS = "disease modified successfully "

# ===================== leave apis =============================
LEAVE_DISPLAY = 'Display-list of all leaves'
LEAVE_TO_BE_MODIFIED = "Data of a leave with given pk "
LEAVE_ADDED = "Leave created successfully."
LEAVE_MODIFY_SUCCESS = "Leave modified successfully "
LEAVE_ACCEPTED = "f'leave accepted, {appointments.count()} appointments cancelled'}"
LEAVE_REJECTED = "Leave rejected"

# ===================== doctor_charges apis =============================
DOCTOR_CHARGES_DISPLAY = 'Display-list of all doctor charges'
DOCTOR_CHARGES_TO_BE_MODIFIED = "Data of a doctor charges with given pk "
DOCTOR_CHARGES_ADDED = "doctor charges created successfully."
DOCTOR_CHARGES_MODIFY_SUCCESS = "doctor charges modified successfully "

# ===================== reports apis =============================
REPORTS_DISPLAY = 'Display-list of all reports'
REPORTS_TO_BE_MODIFIED = "Data of a reports with given pk "
REPORTS_ADDED = "reports created successfully."
REPORTS_MODIFY_SUCCESS = "reports modified successfully "

# ===================== patient reports apis =============================
PATIENT_REPORTS_DISPLAY = 'Display-list of all reports of a patient'
PATIENT_REPORTS_TO_BE_MODIFIED = "Data of a report of a patient with given pk "
PATIENT_REPORTS_ADDED = "reports of a patient posted successfully."
PATIENT_REPORTS_MODIFY_SUCCESS = "reports of a patient modified successfully "

# ===================== insurance apis =============================
INSURANCE_DISPLAY = 'Display-list of all insurances'
INSURANCE_TO_BE_MODIFIED = "Data of an insurance with given pk "
INSURANCE_ADDED = "insurance created successfully."
INSURANCE_MODIFY_SUCCESS = "insurance modified successfully "

# =====================  patient insurance apis =============================
PATIENT_INSURANCE_DISPLAY = 'Display of patient insurance '
PATIENT_INSURANCE_TO_BE_MODIFIED = "Data of a patient insurance with given pk "
PATIENT_INSURANCE_ADDED = "patient insurance added successfully."
PATIENT_INSURANCE_MODIFY_SUCCESS = "patient insurance modified successfully "

# ===================== room apis =============================
ROOM_DISPLAY = 'Display-list of all rooms'
ROOM_TO_BE_MODIFIED = "Data of a room with given pk "
ROOM_ADDED = "room created successfully."
ROOM_MODIFY_SUCCESS = "room modified successfully "

# ===================== room type apis =============================
ROOM_TYPE_DISPLAY = 'Display-list of all room types'
ROOM_TYPE_TO_BE_MODIFIED = "Data of a room type with given pk "
ROOM_TYPE_ADDED = "room type created successfully."
ROOM_TYPE_MODIFY_SUCCESS = "room type modified successfully "

# ===================== patient rooms apis =============================
PATIENT_ROOM_DISPLAY = 'Display-list of all room of a patient'
PATIENT_ROOM_TO_BE_MODIFIED = "Data of a room of a patient with given pk "
PATIENT_ROOM_ADDED = "room of a patient posted successfully."
PATIENT_ROOM_MODIFY_SUCCESS = "room of a patient modified successfully "

# ===================== bill apis =============================
BILL_DISPLAY = 'Display-list of all bills'
BILL_TO_BE_MODIFIED = "Data of a bill with given patient "
BILL_ADDED = "bill created successfully."
BILL_MODIFY_SUCCESS = "bill modified successfully "

# ===================== medicine apis =============================
MEDICINE_DISPLAY = 'Display-list of all medicines'
MEDICINE_TO_BE_MODIFIED = "Data of a medicine to be modified "
MEDICINE_ADDED = "medicine added successfully."
MEDICINE_MODIFY_SUCCESS = "medicine modified successfully "

# ===================== patient medicine apis =============================
ALL_PATIENT_MEDICINE_DISPLAY = 'Display-list of all medicines of a patient'
PATIENT_MEDICINE_DISPLAY = 'Display-list of all medicines of a patient with same doctor'
PATIENT_MEDICINE_TO_BE_MODIFIED = "Data of a medicine of a patient to be modified "
PATIENT_MEDICINE_ADDED = "medicine of a patient added successfully."
PATIENT_MEDICINE_MODIFY_SUCCESS = "medicine of a patient modified successfully "

# ===================== patient medicine time apis =============================
PATIENT_MEDICINE_TIME_DISPLAY_NURSE = 'Display-list of all medicines of a patient with same nurse'
PATIENT_MEDICINE_TIME_DISPLAY_DOCTOR = 'Display-list of all medicines of a patient with same doctor'
PATIENT_MEDICINE_TIME_TO_BE_MODIFIED = "Data of a medicine of a patient to be modified "
PATIENT_MEDICINE_TIME_ADDED = "medicine of a patient added successfully."
PATIENT_MEDICINE_TIME_MODIFY_SUCCESS = "medicine of a patient modified successfully "